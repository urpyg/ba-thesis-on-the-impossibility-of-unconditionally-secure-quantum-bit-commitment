# BA Thesis on the Impossibility of Unconditionally Secure Quantum Bit Commitment
## Latest Build: [impossibleqbc pdf](https://git.scc.kit.edu/urpyg/Ausarbeitung/-/jobs/artifacts/master/file/impossibleqbc.pdf?job=build)
<object >
    <embed src=https://git.scc.kit.edu/urpyg/Ausarbeitung/-/jobs/artifacts/master/file/impossibleqbc.pdf?job=build>
    </embed>
</object>