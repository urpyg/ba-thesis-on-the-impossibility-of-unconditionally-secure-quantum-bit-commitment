\section{Quantum Information and Quantum Computing}\label{sec:quantum-information-and-quantum-computing}
\subsection{State Space}\label{subsec:state-space}
% A short introduction into quantum computing; What is a quantum state? How can we perform operations? What are the limitations?
%(0) Welche Annahmen benötigt man? Was sind grundlegende Theoreme? (zB Principle of deferred measurement)%Schmidt decomposition theorem and Uhlmann’s theorem
%
%(1) Wie funktioniert der Angriff? Was sind die einzelnen Schritte?
%(2) Wie komplex/ aufwendig sind die einzelnen Schritte?
As described in Section~\ref{sec:linear-algebra}, quantum states can be represented as vectors on a complex Hilbert space.
%
The state of a quantum system can be in a superposition of multiple states.
In the vector representation this means, that the vector describing the state of the system in question is a linear combination of vectors describing other states, usually basis vectors.
\begin{Example}\label{ex:single_qubit}
A single qubit in superposition of the computational basis states can be written as
\begin{align}
    \alpha\ket{0} + \beta\ket{1} = \ket{\psi} \equiv \begin{pmatrix*}\alpha\\\beta\end{pmatrix*}\in \mathbb{C}^{2}
\end{align}
%
When measured in that basis, the result will be $\ket{0}$ with a probability of $|\alpha|^2$ and $\ket{1}$ with a probability of $|\beta|^2$.
Like in Example~\ref{ex:photon}, the state describing the qubit \emph{collapses} to one of the basis states.
This means after measuring the qubit to be $\ket{0}$ (or $\ket{1}$ respectively) all future measurements will also show the qubit to be $\ket{0}$ (or $\ket{1}$ respectively).
The details of measurement will be described in Section~\ref{subsec:measurement}.
\end{Example}
\parAfterEnv
Given multiple systems, whose states can be described by the vectors \(\ket{\psi_i}^{\H_i}\) acting on the Hilbert spaces \(\H_i\) respectively, the state of the combined system can be described by the state vector
\begin{align}
  \ket{\psi}^{\H} = \bigotimes_{i}\ket{\psi_i}^{\H_i}\,,
\end{align}
acting on \({\H}\), the tensor product of the Hilbert spaces \(\H_i\).
This notation, to write the system a state belongs to as a superscript to the ket or bra describing that state, will be used throughout the thesis where applicable.

\subsection{Evolution}\label{subsec:evolution}
The evolution of a closed quantum system is described by an operation that transforms the state of the system at time \(t_1\) to the state of the system at time \(t_2\).
A closed system is a system that does not interact with its environment, while an open system is a system that is influenced in some way by its environment.
%Since qubits are written as vectors, operations on them are written as matrices.
\mypar
As quantum states can be represented as vectors on complex Hilbert spaces, linear operators that can be applied on quantum states have a matrix representation and act on the respective Hilbert spaces.
An operation $U$ applied to a quantum state \(\ket{\psi}\) should also result in a valid quantum state $U\ket\psi=\ket{\psi_{\text{new}}}$.
%A vector \(\ket{\psi}= \sum_{i}\alpha_i\ket{b_i}\) describes a valid (pure) state if it fulfills the normalization condition \(1 = \sum_{i}\left\vert\alpha_i\right\vert^2\).
As the restriction to a vector to describe a valid quantum state is that it is of unit length, the restriction for a matrix to describe a valid operation on a state has to be that it is unitary.

\begin{Example}
  One important operation is the Hadamard transformation,
  \begin{align}
    H=\frac{1}{\sqrt{2}}\begin{pmatrix}1&1\\1&-1\end{pmatrix}
  \end{align}
  It transforms the states $\ket0$ and $\ket1$ to a superposition of $\frac{1}{\sqrt{2}}(\ket0+\ket1)$ and $\frac{1}{\sqrt{2}}(\ket0-\ket1)$ respectively.
  \(H\) is also Hermitian, and since it is unitary \(HH = I\).
\end{Example}
\parAfterEnv
In addition to modelling operators as matrices, there exists the \emph{quantum turing machine} and the \emph{quantum circuit} model, which can help to structure quantum operations.
The latter models operations as (reversible) \emph{quantum logic gates} and will be explored in Subsection~\ref{subsec:quantum-circuits}.

\subsection{Measurement}\label{subsec:measurement}
Measurement in Quantum mechanics can be described by a set of measurement operators \(\{M_m\}\), acting on the Hilbert space of the system being measured.
Each index \(m\) corresponds to a possible distinct measurement outcome that can occur.
%
The probability for the measurement outcome \(m\) to occur is
\begin{align}\label{eq:measurment_probability}
  \Pr[m] = \bra{\psi}M^{\dagger}_{m}M_{m}\ket{\psi}\,,
\end{align}
with \(\ket{\psi}\) being the state of the system before being measured.
Has \(m\) occurred, the state of the system changes and the new state can be descried as
\begin{align}
  \ket{\psi_{\text{new}}} = \frac{M_{m}\ket{\psi}}{\sqrt{\Pr[m]}}\,.
\end{align}
All possible outcomes have to be described, and all probabilities have to sum up to one for any a priory system state, this is ensured with the \emph{completeness equation}
\begin{align}\label{eq:completeness_relation}
  \sum_{m}M_{m}^{\dagger}M_{m} = I
\end{align}
\mypar
A special class of measurements are \emph{projective measurements}.
Instead of directly being described by a set of Measurement operators, they are described by an \emph{observable} \(M\), which is composed of a set of Hermitian, orthogonal projectors \(\{P_m\}\).
To be more precise, \(M\) has a spectral decomposition
\begin{align}
  M = \sum_{m}mP_m\,.
\end{align}
The projectors then act analogously to the Measurement operators in the general case, with the probability of receiving result \(m\) being
\begin{align}\label{eq:projector_probability}
  \Pr[m] = \bra{\psi}P_{m}\ket{\psi}\,,
\end{align}
with the a priori system state \(\ket{\psi}\).
The a posteriori system state with the occurrence of \(m\) is
\begin{align}\label{eq:projector_newState}
  \ket{\psi_{\text{new}}} = \frac{P_{m}\ket{\psi}}{\sqrt{\Pr[m]}}\,.
\end{align}
Another name for projective measurements is \emph{von Neumann measurements}.
\enquote{Measuring a state in a basis} is a form of projective measurement, where the projectors correspond to the basis states,
\begin{align}
  P_{m} = \ket{m}\bra{m}\,.
\end{align}
With Equation~\ref{eq:projector_newState}, it can be verified that the new state after basis state \(\ket{m}\) was measured, is in fact the same basis state \(\ket{m}\),
\begin{align}
  \ket{\psi_{\text{new}}} = \frac{\ket{m}\bra{m}\ket{\psi}}{\sqrt{\bra{\psi}\ket{m}\bra{m}\ket{\psi}}} = \ket{m}\,.
\end{align}
With Equation~\ref{eq:projector_probability}, the probability to get the measurement result \(m\) can be calculated to be
\begin{align}
  \Pr[m] = \sqrt{\bra{\psi}\ket{m}\bra{m}\ket{\psi}} = \braket{\psi\vert{}m}\,.
\end{align}
\begin{Example}
  Calculating the probabilities for the single qubit Example~\ref{ex:single_qubit}:
  \begin{align}
    \Pr[0] = \bra{\psi}\ket{0}\bra{0}\ket{\psi} = {\begin{pmatrix*}\alpha\\\beta\end{pmatrix*}^{\dagger}\begin{pmatrix*}1\\0\end{pmatrix*}}{\begin{pmatrix*}1\\0\end{pmatrix*}^{\dagger}\begin{pmatrix*}\alpha\\\beta\end{pmatrix*}} = (1\alpha + 0\beta)(1\alpha^* + 0\beta^*) = \left\vert\alpha\right\vert^2\\
    \Pr[1] = \bra{\psi}\ket{0}\bra{1}\ket{\psi} = {\begin{pmatrix*}\alpha\\\beta\end{pmatrix*}^{\dagger}\begin{pmatrix*}0\\1\end{pmatrix*}}{\begin{pmatrix*}0\\1\end{pmatrix*}^{\dagger}\begin{pmatrix*}\alpha\\\beta\end{pmatrix*}} = (0\alpha + 1\beta)(0\alpha^* + 1\beta^*) = \left\vert\beta\right\vert^2
  \end{align}
\end{Example}
\parAfterEnv
A special case of the measurement formalism is the \emph{POVM} formalism, it focuses on the measurement probability outcomes rather than the state after measurement.
The initialism POVM stands for \emph{\enquote{Positive Operator-Valued Measure}}, which classifies its elements.
%
POVMs are defined to be a set  \(\{E_m\}\) of positive valued operators known as \emph{POVM elements}, that satisfy the completeness relation \(\sum_{m}E_m = I\).
Given a POVM, a set \(\{M_m\}\) of measurement operators can be derived by defining \(M_m \coloneqq \sqrt{E_m}\).
Similarly, given a set of measurement operators \(\{M_m\}\), the POVM \(\{E_m\}\) can be described with the elements \(E_m = M_m^{\dagger}M_m\).
Following the probability equation for measurement operators~\ref{eq:measurment_probability}, the probability for outcome \(m\) is given by \(\Pr(m) = \braket{\psi\vert{}E_m\vert{}\psi}\).
A completeness relation for POVMs can be derived, following the completeness relation~\ref{eq:completeness_relation} for measurement operators.

\subsection{Phase}\label{subsec:phase}
Consider the state \(\ket{\psi} = \alpha\ket{0}+\beta\ket{1}, \alpha, \beta \in \mathbb{C}\) of a single qubit.
As described in Example~\ref{ex:photon} and Sections~\ref{subsec:hilbert-space} and~\ref{subsec:state-space}, the complex numbers \(\alpha, \beta\) encode the amplitude and phase of a wave function in the chosen basis states.
This can be explicitly shown by rewriting the state vector as
\begin{align}
    \ket{\psi} = e^{i\gamma}\left(\cos{\frac{\theta}{2}}\ket{0}+e^{i\varphi}\sin{\frac{\theta}{2}}\ket{1}\right), \gamma,\varphi,\theta\in\mathbb{R}\,.
\end{align}
It is important to point out the relevance of the phase.
The phases, or more precisely phase differences dictate how probability amplitudes of states interfere with each other.
\begin{Example}
  Take the states \(\ket{\psi_1} = \frac{1}{\sqrt{2}}\ket{0} + \frac{1}{\sqrt{2}}\ket{1}\) and \(\ket{\psi_2} = \frac{1}{\sqrt{2}}\ket{0} - \frac{1}{\sqrt{2}}\ket{1}\).
  With both of these states, measuring in the rectilinear basis, the probability of getting \(\ket{0}\) or \(\ket{1}\) respectively is \(\frac{1}{2}\).
  But a system in equal superposition of those states is in the state
  \begin{align}
    \ket{\psi} &= \frac{1}{\sqrt{2}}( \frac{1}{\sqrt{2}}\ket{0} + \frac{1}{\sqrt{2}}\ket{1})+ \frac{1}{\sqrt{2}}(\ket{\psi_2} = \frac{1}{\sqrt{2}}\ket{0} - \frac{1}{\sqrt{2}}\ket{1})\\
    &= \frac{1}{2}(\ket{0}+\ket{0}) + \frac{1}{2}(\ket{1} - \ket{1})\\
    &=\ket{0}\,.
  \end{align}
  Which, when measured in the rectilinear basis always results in \(\ket{0}\).
\end{Example}
\parAfterEnv
Since only the phase difference dictates how the probability amplitudes of states interact with each other, this also means that the global phase will have no effect on measurement outcomes and can safely be ignored.
The state thus can be described by
\begin{align}
  \ket{\psi} = \cos{\frac{\theta}{2}}\ket{0}+e^{i\varphi}\sin{\frac{\theta}{2}}\ket{1}, \varphi,\theta\in\mathbb{R}\,.
\end{align}
\mypar
It can be observed, that the state is dependent of two rational angles.
The \emph{boch sphere} (Figure~\ref{fig:blochsphere}) visualizes this observation.
A one qubit quantum state is illustrated as a point on the surface of the bloch sphere.
Its position is defined by the angle $\varphi$ off the $x$-Axis and the angle $\theta$ off the $y$-Axis.
Observe that since \(\ket{0} = \cos{\frac{0}{2}}+e^{i\varphi}\sin{\frac{0}{2}}\ket{1}\) and \(\ket{1} = \cos{\frac{\pi}{2}}\ket{0}+e^{i\varphi}\sin{\frac{\pi}{2}}\ket{1}, \varphi\in\mathbb{R}\), the orthogonal basis states $\ket{0}$ and $\ket{1}$ lie on opposite sites of the same axis in the bloch-sphere-representation.
So these states that are orthogonal in Hilbert space lie on the same axis in the bloch sphere.
This also visualizes that $\theta$ represents the bias a state has towards one basis state or the other.
In contrast to that, \(\ket{\varphi}\) represents the phase difference between the basis states, which grows less important the closer states move to the poles.
%The probability amplitudes of measurement outcome shows interference
% Because probability amplitudes are associated with wavelike equations, they show wavelike properties, such as interference. Wording from https://docs.microsoft.com/en-us/learn/modules/qsharp-explore-key-concepts-quantum-computing/4-interference-quantum-computing
\begin{figure}[!htb]
  \centering
  \begin{tikzpicture}

    % Define radius
    \newcommand{\radius}{3}

    % Bloch vector
    \draw (0,0) node[circle,fill,inner sep=0.8] (orig) {} -- (\radius/2,\radius/2) node[circle,fill,inner sep=1.4,label=above:$\ket{\psi}$] (a) {};
    \draw[dashed] (orig) -- (\radius/2,-\radius/4) node (phi) {} -- (a);
    \draw (0,-\radius) node[label=below:$\ket{1}$] (one) {};
    \draw (\radius/5*1.5,\radius/3*1.8) node[label=below:$\ket{-}$] (minus) {};
    \draw (-\radius/5*1.1,-\radius/3*1.1) node[label=below:$\ket{+}$] (plus) {};
    \draw (0,\radius*1.1) node[label=above:$\ket{0}$] (zero) {};

    % Sphere
    \draw (orig) circle (\radius);
    \draw[dashed] (orig) ellipse (\radius{} and \radius/3);

    % Axes
    \draw[->] (orig) -- ++(-\radius/5,-\radius/3) node[below] (x1) {\(x\)};
    \draw[->] (orig) -- ++(\radius,0) node[right] (x2) {\(y\)};
    \draw[->] (orig) -- ++(0,\radius) node[above] (x3) {\(z\)};

    %Angles
    \pic [draw=black,text=black,->,"\(\varphi\)"] {angle = x1--orig--phi};
    \pic [draw=black,text=black,<-,"\(\theta\)"] {angle = a--orig--x3};
  \end{tikzpicture}
  \caption{Bloch sphere}
  \label{fig:blochsphere}
\end{figure}
%
The states \(\{\ket{+},\ket{-}\}\) of the diagonal basis are at an \(\theta = 90^\circ\) angle to the computational basis states.
As they are also orthogonal in Hilbert space, they also share an axis on the Bloch sphere.
%Principle of Deferred Measurementthat states that anycomputation done by a quantum circuit using intermediate measurements can be equivalentlyand nearly as efficiently done by a quantum circuit that only has a single measurement at theend


\subsection{Entangled States, Separable States}\label{subsec:entanglementt}
The state of a multi qubit system can be entangled.
When a system is entangled, evolution or measurement on one part of the system instantaneously influences the measurement outcome another, spatially distant, part of the system.
\begin{Example}
An example for an entangled pair of qubits is the EPR-pair $\ket\psi=\frac{1}{\sqrt{2}}(\ket{00}+\ket{11})$.
Here \(\ket{0}\ket{0}\) and \(\ket{1}\ket{1}\) are the only possible results, so if one measures the left qubit to be \(\ket{0}\) (respectively \(\ket{1}\)) it is known that a measurement of the right qubit will always return \(\ket{0}\) (respectively \(\ket{1}\)) as well.
\end{Example}
\parAfterEnv
There are degrees of entanglement a state can have.
Genrally a state $\ket\psi^{A\otimes{}B}$ is \emph{separable}, if and only if there exist states $\ket\psi^A$,$\ket\psi^B$ such that $\ket\psi^{A\otimes{}B} = \ket\psi^A\otimes\ket\psi^B$ and it is entangled if and only if it is not separable.


\subsection{Density Operators}\label{subsec:density-operators}
In a more general form than state vectors, states can also be descried by \emph{density operators}.
\begin{Definition}[Density operator]
  A positive operator \(\rho\) is a density operator, if and only if it has a trace equal to one,
  \begin{align}
    \Tr(\rho) = 1\,.
  \end{align}
\end{Definition}
The state of a system that can be described by the vector \(\ket{\psi}\) on a complex Hilbert space, can equivalently be described by the density operator \(\rho=\ket\psi\bra\psi\).
\mypar
A system whose state can be described by a single density operator \(\rho\) may not necessarily be described by a single vector as well.
A system can be in an \emph{ensemble} of states, this means that for a set \(\{\ket{\psi_i}\}\) of state vectors it is known to be in state \(\ket{\psi_i}\) with probability \(p_i\).
Such a system is said to be in a \emph{mixed state}, and this state can be described by a single density operator
\begin{align}
  \rho = \sum_{i}p_{i}\ket{\psi_i}\bra{\psi_i}\,.
\end{align}
In terms of density operators, if a system is known to be in the state \(\rho_i\) with the probability \(p_i \coloneqq \Pr(\rho_i)\), its density operator is
\begin{align}
  \rho = \sum_{i}p_i\rho_i\,.% = \sum_{i}p_i\ket{\psi_i}\bra{\psi_i}\,.
\end{align}
A detailed definition of ensembles is given in Definition~\ref{def:ensembles}.
A quantum systems state is a \emph{pure state}, if and only if it is known exactly.
That is, when its state can be described by a single vector \(\ket\psi\) or a density matrix \(\rho\) that can be written as $\rho=\ket\psi\bra\psi$.
%
For any pure state \(\rho\), it applies that \(\Tr(\rho^2) = 1\), and for any mixed state \(\rho'\), it applies that \(\Tr(\rho') < 1\).
\mypar
Let \(U\) be the unitary transformation that describes the evolution of a system from the state described by \(\ket{\psi}\) to the state described by \(U\ket{\psi}\).
The same transformation can be applied to a system in a state described by the density operator \(\rho\).
The new state of the system is then described by \(\rho_{\text{new}} = U\rho{}U^{\dagger}\).
\mypar
Given a complete set \(\{M_m\}\) of measurement operators and the a priori system state \(\rho\), the probability for measurement outcome \(m\) to occur is given by
\begin{align}
  \Pr(m) = \Tr(M_m^{\dagger}M_{m}\rho)
\end{align}
and the state of the system after the measurement is
\begin{align}
  \rho_{\text{new}} = \frac{M_{m}\rho{}M_m^{\dagger}}{\Tr(M_m^{\dagger})}\,.
\end{align}
\mypar
Take multiple systems whose states can be described by the density operators \(\rho_i^{\H_i}\), acting on their respective state spaces \(\H_i\).
Analogous to the state vector description, the composite system is described on the state space \(\H = \bigotimes \H_i\), and its state is described by the operator
\begin{align}
  \rho^{\H} = \bigotimes_i \rho_i^{\H_i}\,.
\end{align}
\mypar
Given a composite system in the state \(\rho\), the states of the systems making up the composite system can be described using the partial trace over \(\rho\).
%The resulting operator is called the \emph{reduced density operator}
%
As with state vectors, when describing states of different or composite systems, a superscript is added to the density operator, indicating the respective system it acts on.
%
Let \(\H = {\H}_\mathcal{A} \otimes {\H}_\mathcal{B}\) be the state space of the composite system of the systems \(\mathcal{A}\) and \(\mathcal{B}\) with the state spaces \({\H}_\mathcal{A}\) and \({\H}_\mathcal{B}\) respectively.
Denote \(\rho^{\H}\) the state of the composite system, then the resulting \emph{reduced density operator}of system \(\mathcal{A}\) is
\begin{align}
  \rho^{{\H}_{\mathcal{A}}} = \Tr_{\mathcal{B}}(\rho^{\H})\,,
\end{align}
and the reduced density operator for system \(\mathcal{B}\) is
\begin{align}
  \rho^{{\H}_{\mathcal{B}}} = \Tr_{\mathcal{A}}(\rho^{\H})\,.
\end{align}
With \(\Tr_{\mathcal{P}}, \mathcal{P}\in\{\mathcal{A},\mathcal{B}\}\) being the partial trace over system \(\mathcal{P}\).
%
Even when \(\rho^{\H}\) is a pure state, in many cases \(\rho^{{\H}_{\mathcal{A}}}\) or \(\rho^{{\H}_{\mathcal{B}}}\) are mixed states.

\begin{Theorem}[Schmidt decomposition\footnotemark]\label{thm:schmidt}
Let \(\ket{\psi}\) be a pure state of a system \(\H_1\otimes\H_2\), and let \(k \coloneqq \min(\dim(\H_{1}),\dim(\H_{2}))\).
%
Then there exist orthonormal bases \(\{\ket{e_i}\}\) and  \(\{\ket{f_i}\}\) of systems \(\H_1\) and \(\H_2\) respectively, and coefficients \(\{0\leq \lambda_i\in\mathbb{R}\colon \sum_{i = 1}^{k}\lambda_i = 1\}\), such that \(\ket{\psi}\) can be written as
\begin{align}
  \ket{\psi} = \sum_{i = 1}^{k} \lambda_i\ket{e_i}\ket{f_i}\,.
\end{align}
This is called the \emph{Schmidt decomposition of \(\ket{\psi}\).}
\end{Theorem}
\footnotetext{This theorem is based on the phrasing~\textcite[109]{nielsenQuantumComputationQuantum2010a} give of~\textcite[\textsection 15]{schmidtZurTheorieLinearen1907}.}
To find such orthonormal states, the following corollary can be used.
\begin{Corollary}[Schmidt polar form\footnotemark]\label{col:schmidt}
Let \(\ket{\psi}\) be a pure state of a system \(\H_1\otimes\H_2\).
Let \(\rho_1 = \Tr_{\H_2}(\ket{\psi}\bra{\psi})\) and \(\rho_2 = \Tr_{\H_1}(\ket{\psi}\bra{\psi})\) the reduced density operators of systems \(\H_1\) and \(\H_2\) respectively, and let \(k \coloneqq \min(\dim(\H_{1}),\dim(\H_{2}))\).
%
Then \(\rho_1\) and \(\rho_2\) have the same nonzero eigenvalues with the same multiplicities.
Following that, if one space is larger than the other, the extra dimensions are made up with zero eigenvalues.
Let \(\ket{e_i}\) and \(\ket{f_i}\) be orthonormal eigenvectors of \(\rho_1\) and \(\rho_2\) respectively.
Then \(\ket{\psi}\) can be written as
\begin{align}
  \ket{\psi} = \sum_{i = 1}^{k} \sqrt{\lambda_i}\ket{e_i}\ket{f_i}\,.
\end{align}
This is sometimes also referred to as the \emph{Schmidt polar form}.
\end{Corollary}
\footnotetext{This corollary is based on the application~\textcite[5]{HUGHSTON199314} give of~\textcite[\textsection 15]{schmidtZurTheorieLinearen1907}.}

\subsection{Distance Measures}\label{subsec:distance-measures}
Quantum distance measurements aim to describe how similar two states are, or how difficult it is to tell them apart.
As it is not possible to read the exact amplitude and phase of a qubit, but only to measure it and receive some result with some probability, quantum distance measures are in some regards similar to measures differentiating probability distributions.
\mypar
One such measure for difference in quantum states is the trace distance between those states.
\begin{Definition}[Trace distance]
  The \emph{trace distance} between two states \(\rho\) and \(\sigma\) is
  \begin{align}
    D(\rho,\sigma) \coloneqq \frac{1}{2}\Tr\left\vert{}\rho - \sigma\right\vert\,,
  \end{align}
  with \(\left\vert{} A \right\vert{} \coloneqq \sqrt{A^{\dagger}A}\)
\end{Definition}
The trace distance between single qubit states can be visualized well on the bloch sphere, it is equal to \(\frac{1}{2}\) the Euclidean distance between those states on it. %\todo[inline, color=white]{To relate it to differentiating probability distributions, the trace distance between two states is equal to the difference in probabilities that a measurement outcome with POVM element \(P\) my occur. Thus, if two density operators are close in trace distance, then any measurement performed on those quantum states will give rise to probability distributions which are close together in the classical sense of trace distance.}
\parAfterEnv
Another important measure is the fidelity.
\begin{Definition}[Fidelity]
  The \emph{fidelity} between two states \(\rho\) and \(\sigma\) is
  \begin{align}
    F(\rho,\sigma) \coloneqq \Tr\sqrt{\rho^{\sfrac{1}{2}} \sigma \rho^{\sfrac{1}{2}}}\,.
  \end{align}
  For all
  \begin{equation}\label{eq:filedity_bounds}
      \rho, \sigma, 0 \leq F(\rho, \sigma) \leq 1\,,
  \end{equation}
  where equality in~\ref{eq:filedity_bounds} is achieved on the left side if and only if \(\rho\) and \(\sigma\) are orthogonal, and on the right side if and only if \(\rho = \sigma\).
\end{Definition}
\parAfterEnv
So a higher fidelity between states means that they are harder to tell apart, while a higher trace distance implies that the states are less similar to each other.
Both fidelity and trace distance are invariant under unitary transformations that are applied to both states.
\mypar
Given an unknown state \(\ket{\psi_i}\) from a set of known orthonormal states \(\{\ket{\psi_i}\}\), it is possible to determine which state of the set was given by performing an appropriate measurement.
However, if given a state \(\ket{\phi_i}\) from a set of known non-orthogonal states \(\{\ket{\phi_i}\}\), it is not possible to determine which state was given with absolute certainty .
The amount of information that is possible to gain about \(\ket{\phi_i}\) is dependent of how close \(\{\ket{\phi_i}\}\) are to each other.
The closer they are, the harder they are to differentiate.
This observation is related to the \hyperref[thm:no-cloning]{no-cloning theorem}.
%One of the most fundamental theorems is the
\begin{Theorem}[No-cloning]\label{thm:no-cloning}
Non-orthogonal quantum states cannot be copied.
That is, for any given quantum device that performs a valid operation modelled by a unitary operation \(U\): If \(U\) is able to copy \(\ket{\psi}\) and \(\ket{\phi}\) as follows
\begin{align}
  &\ket{\psi}\ket{\xi_\text{target}}U = \ket{\psi}\ket{\psi}\\
  &\ket{\phi}\ket{\xi_\text{target}}U = \ket{\phi}\ket{\phi}\,,
\end{align}
then \(\ket{\psi}\) and \(\ket{\phi}\) are either the same state, or orthogonal to each other.
\end{Theorem}
\mypar
See also the no-cloning theorem as given by~\textcite[532]{nielsenQuantumComputationQuantum2010a}.


\subsection{Decoherence}\label{subsec:decoherence2}
While in quantum information usually closed systems are considered, in reality most systems are influenced by their environment.
Consider a system in a pure a state represented by the matrix \(\rho\), the diagonal elements are called \emph{populations} while the off-diagonal elements are called \emph{coherences}.
When interacting with the environment the populations stay unaffected, while the coherences get multiplied with a factor of modulus of less than one and thus get suppressed with more interactions over time~\autocite[224]{hornbergerIntroductionDecoherenceTheory2009}.
After the interaction, the system is not in a pure state anymore.
The populations measure the probabilities that the system is in either state of the basis states.
The coherences measure the amount of quantum interference between the states~\autocite[309]{brandtQuantumDecoherenceQubit2003} and characterize the ability of the system to display a superposition between basis states~\autocite[224]{hornbergerIntroductionDecoherenceTheory2009}.
This loss of coherence occurs in a special basis, determined by the type of environmental interaction.

\begin{Example}
  Let the state of a system be the pure state \(\ket{\psi} = \frac{1}{\sqrt{2}}\ket{0} + \frac{1}{\sqrt{2}}\ket{1}\).
  The corresponding density operator is
  \begin{align}
    \rho = \frac{1}{2}\begin{pmatrix}1&1\\1&1\end{pmatrix}\,.
  \end{align}
  Through interaction with the environment, \(\rho\) eventually loses its coherences.
  The system is now in the mixed state
  \begin{align}
    \rho' = \frac{1}{2}\begin{pmatrix}1&0\\0&1\end{pmatrix} = \frac{1}{2}\begin{pmatrix}1&0\\0&0\end{pmatrix} + \frac{1}{2}\begin{pmatrix}0&0\\0&1\end{pmatrix} = \frac{1}{2}\ket{0}\bra{0} + \frac{1}{2}\ket{1}\bra{1}\,.
  \end{align}
  Thus it is not in superposition, but in an ensemble of the states \(\ket{0}\) and \(\ket{1}\), each with probability \(\frac{1}{2}\), analogous to a system that has been measured but whose result has been forgotten.
\end{Example}
%
Decoherence is also the reason why large scale systems do not seem to show quantum behaviour.


%\subsection{Theorems}\label{subsec:theorems}



%Theorems and principles that are needed for the construction include \todo\\
%%Consider two states in a system and associated states in a copy of that system.
%%Uhlmann’s theorem describes the \emph{fidelity} (a measurement of distance) of the two states in the first system, as a relation of the states in the second system.
%n short, states that applying transformation to one entangled state also changes measurement outcome of the other.\\ %BIG TODO See https://ocw.tudelft.nl/course-lectures/2-4-1-using-entanglement-share-classical-secret/?course_id=22166 and https://ocw.tudelft.nl/course-lectures/2-2-2-uhlmanns-theorem/?course_id=22166
%%Let $\ket{i}_A$ and $\ket{i}_B$ form a basis for the systems $\H_A$ and $\H_B$ respectively.
%%Schmidt's decomposition theorem states that it is possible for a pure state of the composite System $\ket\psi_{AB}\in \H_A\otimes \H_B$ to be written as a weighted sum of the basis states of $\H_A$ and $\H_B$, $\ket\psi_{AB}\equiv \sum_{i=0}^{d-1} \lambda_i\ket{i}_A\ket{i}_B$, $\lambda_i\in\mathbb{R}^+$.


%Working on the circuit model of quantum computing, it would be possible to measure qubits in different parts of the circuit.
%It would also be possible to measure them before they have entered or after they have exited the circuit.
%Surprisingly, it does not matter for the outcome, when the qubits are measured.
%This is known as the principle of deferred measurement.

\subsection{Quantum Circuits}\label{subsec:quantum-circuits}
Computing is fundamentally reversible.
However, in classical computing some information is not kept track of, so that it is not reversible anymore.
This is not the case for quantum computing, as operations are carried out on individual particles or a set of individual particles.
This is also modeled in the algebraic sense: operations are unitary matrices and thus invertible.
In fact, the inverse of a unitary matrix is its complex conjugate.
\mypar
The quantum circuit model models operations as quantum logic gates, similar to classical circuits that use classical logic gates.
There are some differences between quantum and classical circuits, as per the \hyperref[thm:no-cloning]{no-cloning theorem}, fanout operations are not permitted.
As quantum operations have to be reversible, and the classical \textsc{or} operation is not, fan-in is not permitted either.
Time in a quantum circuit diagram is modeled to go in one direction.
In this thesis, it is always modeled going from left to right.
\mypar
In a quantum circuit diagram single qubits are presented on quantum \enquote{wires}, classical wires for classical bits are drawn as double stroked wires.
A quantum circuit usually consists of quantum and classical wires, quantum logic gates and measurement operators.
On the left side of a quantum circuit, for each quantum wire, the state that wire starts in can be denoted.
Quantum logic gates can be unitary operations, \textsc{controlled-not}-gates, or controlled unitary operations.
A controlled operation means that the operation is only applied to the target qubit, if the control qubit is one.
Gates that have multiple control or target qubits are also valid quantum logic gates.
Those parts of quantum circuit diagrams are illustrated in Figure~\ref{fig:circ:components}.
\mypar
\begin{figure}[!htb]
  \begin{subfigure}[b]{.5\linewidth}
    \centering
    %! suppress = EscapeAmpersand
    \begin{quantikz}
      \lstick{\(\ket{0}^{\otimes n}\)}&\gate{U} \qwbundle[alternate]{}& \qwbundle[alternate]{}
    \end{quantikz}
    \caption{Unitary operation}
  \end{subfigure}
  \begin{subfigure}[b]{.5\linewidth}
    \centering
    %! suppress = EscapeAmpersand
    \begin{quantikz}
      \lstick{\(\ket{\psi}\)}&\qw & \ctrl{1} & \qw\\
      \lstick{\(\ket{0}\)}&\qw & \gate{U} & \qw
    \end{quantikz}
    \caption{Controlled unitary operation}
  \end{subfigure}
  \begin{subfigure}[b]{.5\linewidth}
    \centering
    %! suppress = EscapeAmpersand
    \begin{quantikz}
      \lstick{\(\ket{\psi}\)}&\qw&\meter{}&\cw
    \end{quantikz}
    \caption{Measurement operator}
  \end{subfigure}
  \begin{subfigure}[b]{.5\linewidth}
    \centering
    %! suppress = EscapeAmpersand
    \begin{quantikz}
      \lstick{\(\ket{\psi}\)}& \ctrl{1} &\qw \\
      \lstick{\(\ket{\phi}\)}& \targ{}&\qw
    \end{quantikz}
    \caption{\textsc{controlled-not}}
  \end{subfigure}
  \caption{Components of a quantum circuit}
  \label{fig:circ:components}
\end{figure}
%
All quantum circuits can be rewritten to use only a discrete subset of quantum logic gates, so called \emph{universal gates}. % with which all possible quantum operations can be modeled.
The number of those universal gates in a circuit can be a good metric of how complex the operation being modelled, is.

\begin{Theorem}[Deferred measurement]
  The usage of a measuring apparatus at any point of the quantum circuit can be moved to the end of said circuit, without changing its behavior
\end{Theorem}

\begin{Theorem}[Implicit measurement]
  Any unterminated quantum wire is assumed to be measured.
  This does not change the circuits' behaviour.
\end{Theorem}
\mypar
Compare to principles of deferred and implicit measurement as given by~\textcite[p.~186 et seq.]{nielsenQuantumComputationQuantum2010a}.