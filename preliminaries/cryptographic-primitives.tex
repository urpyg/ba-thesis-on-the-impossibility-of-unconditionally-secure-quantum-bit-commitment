\section{Cryptographic Primitives}\label{sec:cryptographic-primitives}
In this section some cryptographic primitives and their security conditions will be presented.

\begin{Definition}[Adjectives for security proprties]
  A property in (quantum) cryptography is defined to hold \emph{unconditionally}, if it holds against a cheater with no limit on time, space or technology available to them.
  \mypar
  A protocol is
  \begin{itemize}
    \item \emph{perfectly} secure, if its security properties hold in any case against any attacker.
    \item It is \emph{statistically} secure, if the probability of an unconditional attacker succeeding is negligible in its security parameter.
    \item It is \emph{computationally} secure, if its security depends on the assumption that a certain (quantum) computation is hard to perform.
  \end{itemize}
\end{Definition}
%\begin{Definition}[Neglible]
%  A nonzero value is said to be negligible in the security parameter \(n\),\(\text{negl}(n)\), if it nears zero faster than the inverse of every polynom.
%  \begin{align}
%    \exists n_0: \forall n > n_0, k\colon\quad \text{negl}(n) < n^{-k}\,.
%  \end{align}
%\end{Definition}

\subsection{Bit Commitment}\label{subsec:bit-commitment}
Bit commitment is an asymmetric cryptographic primitive between two parties.
One party wants to commit themselves to a bit and desires the other party not to be able to determine the contents of this commitment before they unveil it.
Meanwhile, the other party is interested in the first party not being able to change their mind about the commitment.
Throughout this thesis, the committing party will be called \emph{Alice}, and the party receiving the commitment will be called \emph{Bob}.
\mypar
Often the analogy of a strong-box is used, where Alice writes her bit on a piece of paper and puts it in a box which she then gives to Bob and thus commits herself to this bit.
Bob is not able to open this box without the key, which Alice provides when she decides to unveil the committed bit.
A bit commitment protocol thus consists of two phases, the \emph{commit} phase and the \emph{unveil} phase.
\mypar
During the \emph{commit} phase, Alice decides on a bit to commit and encodes it in some way.
The encoded bit is called a \emph{commitment}.
She then sends this commitment to Bob.
\mypar
In the \emph{unveil}, sometimes also called \emph{decommitment} phase, Alice provides information to Bob that indicate the bit she committed herself to and that makes it possible for Bob to verify, whether she really did commit herself to that bit.
Bob then either accepts that commitment or rejects it.
If he accepted the bit he then announces what he thinks the committed bit was.
\mypar
It is sometimes useful to model Alice's and Bob's actions these two phases through procedures \(\commit(b)\) and \(\unveil(c_b)\).
Procedure \(\commit(b)\) models the commit phase and takes the bit to commit as an input.
It returns the commitment \(c_b\).
The unveil phase is modelled through \(\unveil(c_b)\) which takes the commitment \(c_b\) as an input and either returns a bit \(b\) or \(\bot\) on error, or if cheating has been detected.
\mypar
During the commit and unveil phase additional communication and computation can occur.
So as a more broad description, the \emph{commit} phase is the phase from the start of the protocol, including any setup procedures, until Bob has received and processed a commitment of some form, and the \emph{unveil} phase is the phase at the end of the protocol where the contents of the commitment are unveiled to Bob and he verifies its correctness.
\mypar
Sometimes the notion of a \emph{holding} phase is useful.
This phase describes processes in the time after the commit phase, but before the unveil phase.
In most honest protocols nothing happens in this phase, therefore it is often omitted.

\begin{Definition}[Correctness of a bit commitment]
  A bit commitment is \emph{correct}, if in any case in which both parties are honest and Alice committed to \(b\in\{0,1\}\), Bob accepts the commitment and is convinced that Alice committed to \(b\).
\end{Definition}
\begin{Definition}[Perfectly and statistially binding for classical bit commitments]
  A bit\break\hfill commitment scheme is \emph{perfectly binding}, if and only if every commitment that can be revealed to be \(b \in \{0,1\}\) cannot be revealed to be \(1 -b\).
  %
  A bit commitment scheme with a security parameter \(n\) is \emph{statistically binding}, if and only if given any commitment \(c\), the probability that Alice successfully reveals 0 in the unveil phase minus the probability that Alice successfully reveals 1 is negligible in the security parameter.
  \begin{align}
    \forall\A(\cdot) \rightarrow c \colon\abs{\Pr\left[\unveil(c) \rightarrow 0\right] - \Pr\left[\unveil(c) \rightarrow 1\right]} \leq \text{negl}(n)\,.
  \end{align}
\end{Definition}

\begin{Definition}[Perfectly and statistically concealing for classical bit commitments]
  A\break\hfill bit commitment scheme is \emph{perfectly concealing}, if and only if any commitment produced by \(\commit(\cdot)\) contains no information about the committed bit.
%
  A bit commitment scheme with security parameter \(n\) is \emph{statistically concealing}, if and only if for any two commitments \(c_0\) and \( c_1\), that can honestly be revealed to \(0\) and \(1\) respectively, the probability that Bob is able to correctly differentiate between the two is negligible in the security parameter.
  \mypar
  Note, a commonly used synonym for \emph{concealing} is \emph{hiding}.
\end{Definition}
%
\begin{Definition}[Perfect and unconditional security for classical bit commitments]
A bit\break\hfill commitment scheme is \emph{perfectly secure}, if and only if it is perfectly concealing and perfectly binding.
It is \emph{unconditionally secure}, if and only if it is perfectly or statistically concealing and perfectly or statistically binding against an unconditional adversary.
\end{Definition}
It is known, that perfect and unconditionally secure bit commitment is impossible in the classical world~\parencite[24]{kilianFoundingCrytpographyOblivious1988}.