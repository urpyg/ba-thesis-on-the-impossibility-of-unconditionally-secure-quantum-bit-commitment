\section{Linear Algebra}\label{sec:linear-algebra}
In quantum information and quantum computation, the fundamental unit of information is the quantum bit (\emph{qubit}), analogous to the fundamental unit of information in classical information theory, which is the \emph{bit}.
In the same way that classical bits can have states that are described by the numbers \(0\) and \(1\), qubits can have states that are described by the vectors \(\ket{0}\) and \(\ket{1}\).
Unlike classical bits, but like in Example~\ref{ex:photon}, states of qubits can be described as a superposition of multiple basis states.

\begin{Example}\label{ex:photon}
The wave function of a photon can be described as $\alpha\ket{\uparrow} + \beta\ket{\rightarrow}$, whereas $\alpha$ and $\beta$ are complex numbers, that encode the amplitude and phase of a wave function in the $\uparrow$ or $\rightarrow$ direction respectively.
It is said that the photons state is in a \emph{superposition} of the states \(\ket{\uparrow}\) and \(\ket{\rightarrow}\).

When measured, the photon will have either a vertical polarization with a probability of $|\alpha|^2$ or a horizontal polarization with a probability of $|\beta|^2$.
\end{Example}
\parAfterEnv
This example shows some physical background of what is going to follow.
To understand how to model the behaviour of a quantum mechanical system in a way that is useful for quantum information and quantum computation, first some mathematical foundations have to be laid out.
The concepts laid out in this section will then be put in relation to quantum computing in Section~\ref{sec:quantum-information-and-quantum-computing}.
%
The definitions in this section are based on~\textcite{nielsenQuantumComputationQuantum2010a}.
\subsection{Hilbert Space}\label{subsec:hilbert-space}
A vector space with an inner product is called an \emph{inner product space}.
Furthermore an inner product space that is a complete metric space is called a \emph{Hilbert space}.
All finite dimensional vector spaces are complete.
In quantum computation and quantum information, finite complex vector spaces are used to describe quantum states as vectors.
So in this context, inner product spaces are the same as Hilbert spaces.
%Hilbert spaces are denoted \(\H\).
\mypar
The \emph{dirac} notation is used to describe vectors of the \(n\)-dimensional Hilbert space \(\H \coloneqq \mathbb{C}^n\) of vectors of complex numbers.
It consists of the elements $\ket{\cdot}$ and  $\bra{\cdot}$, called \emph{ket} and \emph{bra} respectively.
When writing \(\ket{\psi}\), or \(\bra{\psi}\), \(\psi\) is the label assigned to the vector.
Given
\begin{align}
  \ket{\psi}:\equiv\begin{pmatrix}\psi_1\\\vdots\\\psi_n\end{pmatrix}\,,
\end{align}
it applies that
\begin{align}
  \bra{\psi} \equiv \begin{pmatrix}\psi_1^*&\cdots&\psi_n^*\end{pmatrix}\,,
\end{align}
where $\psi_i^*$ denotes the complex conjugate of $\psi_i$.
This means that \(\bra{\psi} = (\ket{\psi}^*)^T\).
\mypar
Dirac notation is useful for the \emph{inner product} of two vectors in this complex vector space.
The inner product of the vectors $\ket{\psi}$ and $\ket\phi$ is written as \(\braket{\psi|\phi} \) and calculated as
\begin{align}
  \braket{\psi|\phi} \coloneqq \bra{\psi}\ket\phi = \sum_{i}{\psi_i^{*}\phi_i}\,.
\end{align}
With the inner product defined, it is also possible to define the norm as
\begin{align}
  \Vert\ket{\phi}\Vert = \sqrt{\braket{\phi\vert\phi}}\,.
\end{align}
%
With respect to this norm, only vectors with unit length, so \(\Vert\ket{\psi}\Vert = 1\), describe valid states of qubits.
\mypar
As Hilbert spaces are vector spaces, different bases can be chosen so that linear combinations of those basis vectors span the Hilbert space.
A basis \(\{\ket{b}_1,\dots,\ket{b}_k\}\) is \emph{orthonormal}, if and only if its vectors are normalized and orthogonal to each other, so
\begin{align}
  \braket{b_i|b_j} = \delta_{ij} \coloneqq
  \begin{cases}
    0,& i \neq j\\
    1, &i = j
  \end{cases}
\end{align}
for each two basis states $\ket{b_i}$,$\ket{b_j}$.
Such a basis spans a complex Hilbert space of dimension $k$.
Any orthonormal basis \(\{\ket{b}_1,\dots,\ket{b}_k\}\) for a vector space \(V\) fulfills the \emph{completeness relation}
\begin{align}\label{eq:completeness}
  \sum_{i} \ket{b}_i = I\,.
\end{align}
\parAfterEnv
The two-dimensional complex Hilbert space is used to describe the state of single qubits, and the basis usually chosen for this space is the so-called \emph{computational}, or \emph{rectilinear} basis.
It consists of
\begin{align}
  \ket{0} \equiv \begin{pmatrix*}1\\0\end{pmatrix*},\ket{1} \equiv \begin{pmatrix*}0\\1\end{pmatrix*}\,.
\end{align}
Another base for the same Hilbert space is the \emph{diagonal} basis \(\{\ket{+},\ket{-}\}\) with
\begin{align}
  \ket{+} \coloneqq \frac{1}{\sqrt{2}}(\ket0+\ket1), \ket{-} \coloneqq \frac{1}{\sqrt{2}}(\ket0-\ket1)\,.
\end{align}
\parAfterEnv
To differentiate in which basis a vector is referred to in, the notation \(\ket{\psi}_{\theta}\) with \(\theta\in{+,\times}\) is used.
\begin{align}
  \ket{0}_+ \coloneqq& \ket{0},& \ket{1}_+ \coloneqq& \ket{1},\\
  \ket{0}_{\times} \coloneqq& \ket{+},& \ket{1}_{\times} \coloneqq& \ket{-}\,.
\end{align}
If the index \(\theta\) is not specified, \(\theta = +\) is assumed.
%
The naming and notation for the bases used here follows the naming and notations of~\textcite{IEEE1984}.

\subsection{Operators}\label{subsec:operators}
A \emph{linear operator} on a vector space \(V\) is a function \(A\colon V \rightarrow V\) for which
\begin{align}
  A\left( \sum_{i}a_{i}\ket{\psi_i} \right) = \sum_{i}a_{i}A\left(\ket{\psi_i}\right), \forall \ket{\psi_i} \in V, a_i \in \mathbb{C}\,.
\end{align}
Matrices are linear operators and linear operators can have matrix representation~\autocite[64]{nielsenQuantumComputationQuantum2010a}.
So evaluating the matrix multiplication between the matrix representation of the operator \(A\) and vector \(\ket{\psi}\) is equivalent to applying operator \(A\) to vector \(\ket{\psi}\), \(A(\ket{\psi}) = A\ket{\psi}\).
\mypar
On a Hilbert space the \emph{Hermitian conjugate} or \emph{adjoint} of a matrix \(A\) is written as \(A^{\dagger}\) and is the transpose of the complex conjugate \(A^*\) of \(A\),
\begin{align}
  A^{\dagger} = (A^{*})^T = ((a_{ij})^*)^T = (a_{ji}^*)\,.
\end{align}
This application of the adjoint on a matrix is derived from the definition of the adjoint for an operator.
%, as the adjoint of a matrix that represents an operator on a Hilbert space, is a matrix representation of the adjoint of that operator.
%
The adjoint of an operator on a Hilbert space is defined with the inner product of that hilbert space.
\begin{Definition}[Adjoint]
Let \(A\) be an operation on a Hilbert space \(\H\), and for all \(\ket{\phi},\ket{\psi}\in \H\) let \(\ket{\psi'} \coloneqq A(\ket{\psi})\) and \(\ket{\phi'} \coloneqq A^{\dagger}(\ket{\phi})\), then \(A^{\dagger}\) is the only operation such that
\begin{align}
  \braket{\phi\vert{}\psi'} = \braket{\phi'\vert{}\psi} = \braket{\phi\vert{}A\vert{}\psi}\,.
\end{align}
\end{Definition}
%
For every matrix representation of operator \(A\), the adjoint of that matrix is a matrix representation of operator \(A^{\dagger}\).

%An operator \(A\) is \emph{Normal} if \emph{Hermitian} or \emph{self adjoint}, if \(A = A^{\dagger}\).
\begin{Definition}[Hermitian, unitary and normal operators]
  \begin{align*}
    \text{An operator }A \text{ is }
    \begin{cases}
      \text{\emph{Hermitian} or \emph{self-adjoint}, iff }& A = A^{\dagger}\\
      \text{\emph{unitary}, iff }& AA^{\dagger} = I\\
      \text{\emph{normal}, iff }& AA^{\dagger} = A^{\dagger}A.
    \end{cases}
  \end{align*}
  \vspace{1sp}
\end{Definition}
Hermitian and unitary operators are normal.
\begin{Definition}[Positive operators]
  A Hermitian operator \(A\) is \emph{positive}, if and only if for all vectors \(\ket{v}\) on the operators Hilbert space, \(\braket{v\vert{}A\vert{}v \geq 0}\).
  It is \emph{positive definite}, if and only if for all vectors \(\ket{v} \neq 0\) on the operators Hilbert space, \(\braket{v\vert{}A\vert{}v > 0}\).
\end{Definition}


\begin{Definition}[Projector]
  Given the \(n\)-dimensional vector space \(V\) and an \(m\)-dimensional subspace \(W\) of \(V\), it is possible to construct an orthonormal basis \(\ket{v}_{1},\ldots,\ket{v}_{n}\) for \(V\), such that \(\ket{v}_{1},\ldots,\ket{v}_{m}\) is a basis for \(W\).
  The operator
  \begin{align}
    P \equiv \sum_{i = 1}^{m}{\ket{v_i}\bra{v_i}}
  \end{align}
  is called the \emph{projector} to subspace \(W\).
\end{Definition}
Projectors are Hermitian as \((\ket{\psi}\bra{\psi})^{\dagger} = \ket{\psi}\bra{\psi}\) for any vector \(\ket{\psi}\).
\mypar
Let \(A\) be a linear operator, that acts on a vector space \(V\).
The \emph{eigenvectors} and corresponding \emph{eigenvalues} of  \(A\) are the eigenvectors and corresponding eigenvalues of any of the matrix representations of that linear operator \(A\).
The eigenvectors and eigenvalues do not depend on a matrix representation, but the linear operator itself.
Every eigenvalue \(\lambda\) has an \emph{eigenspace}.
It is the vector space that it is spanned by the eigenvectors to which the eigenvalue \(\lambda\) corresponds to, it also is a subspace of the operators vector space \(V\).

\begin{Definition}[Diagonal representation, diagonalizable]
  Let \(A\) be an operator on a vector space \(V\) and \(\{\ket{v_i}\}\) an orthonormal set of eigenvectors of \(A\) with the eigenvalues \(\lambda_i\).
  A \emph{diagonal representation}, or \emph{orthonormal decomposition}, of \(A\) is a representation of the form
  \begin{align}
    A = \sum_{i}\lambda_{i}\ket{v_i}\bra{v_i}
  \end{align}
  Not every operator has such a representation.
  Operators for which a diagonal representation exists are called \emph{diagonalizable}.
\end{Definition}
The following theorem is based on~\textcite[72]{nielsenQuantumComputationQuantum2010a}.
\begin{Theorem}[Spectral Decomposition]
  Any diagonalizable operator is normal.
  Any normal operator \(M\) on a vector space \(V\) is diagonalizable.
  The orthonormal eigenvectors \(\{\ket{v_i}\}\) that make up a decomposition of \(M\) are a basis for \(V\).
\end{Theorem}
\parAfterEnv
As \(\{\ket{v_i}\}\) is an orthonormal basis for \(V\), and \(\ket{v_i}\) are eigenvectors of \(M\) with eigenvalues \(\lambda_i\), \(P_i \coloneqq \ket{v_i}\bra{v_i}\) are projectors to the respective eigenspaces of \(M\).
So \(M\) can be decomposed into
\begin{align}
  M = \sum_{i}\lambda_{i}P_i\,.
\end{align}
Since  \(\{\ket{v_i}\}\) is an orthonormal basis for \(V\), \(P_i\) fulfill the completeness relation
\begin{align}
  \sum_{i}P_i = I\,.
\end{align}
In addition to that, an orthonormality relation for \(P_i\) can be formulated as
\begin{align}
  P_{i}P_{j} = \ket{v_i}\overbrace{\braket{v_i\vert{}v_j}}^{=\delta_{ij}}\bra{v_j} = \delta_{ij}P_i\,,
\end{align}

\subsection{Tensor Product}\label{subsec:tensor-product}
To combine multiple Hilbert spaces the \emph{tensor product} can be used.
\begin{Definition}[Tensor product]
  Let \(\H_1\) and \(\H_2\) be \(n\) and respectively \(m\)-dimensional Hilbert spaces.
  Then \(\H = \H_1\otimes\H_2\) is a Hilbert space of dimension \(nm\) with the following properties.
  For its vectors:
  \begin{alignat}{2}
    \forall \ket{\psi_1}\in\H_1\ket{\psi_2}\in\H_2\colon&\quad \ket{\psi_1}\otimes\ket{\psi_2} \eqqcolon \ket{\psi} \in \H\\
    \forall z \in \mathcal{C},\ket{\psi_1}\in\H_1\ket{\psi_2}\in\H_2\colon&\quad z (\ket{\psi_1}\otimes\ket{\psi_2})= (z\ket{\psi_1})\otimes\ket{\psi_2} = \ket{\psi_1}\otimes(z\ket{\psi_2})\\
    \forall\ket{\psi_1},\ket{\psi'_1}\in\H_1\ket{\psi_2}\in\H_2\colon&\quad (\ket{\psi'_1}+\ket{\psi_1})\otimes\ket{\psi_2}= \ket{\psi_1}\otimes\ket{\psi_2} + \ket{\psi'_1}\otimes\ket{\psi_2}\\
    \forall\ket{\psi_1}\in\H_1\ket{\psi_2},\ket{\psi'_2}\in\H_2\colon&\quad \ket{\psi_1}\otimes(\ket{\psi_2}+\ket{\psi'_2})= \ket{\psi_1}\otimes\ket{\psi_2} + \ket{\psi_1}\otimes\ket{\psi'_2}\,.
  \end{alignat}
  For any operator \(A\) on \(\H_1\) and any operator \(B\) on \(\H_2\), \(C \coloneqq A \otimes{} B\) is an Operator on \(\H\) and
  \begin{align}
    \forall \ket{\psi_1}\in\H_1\ket{\psi_2}\in\H_2\colon\quad C(\ket{\psi_1}\otimes{}\ket{\psi_2}) \coloneqq A\ket{\psi_1}\otimes{}B\ket{\psi_2}\,.
  \end{align}
\end{Definition}
\parAfterEnv
It is possible to generalize this definition to vector spaces and operators that map between different vector spaces, however this is not needed for the thesis and is therefore left out.
%\mypar
%When multiple qubits are combined in a register, the vector that describes the state of that register can be calculated using the tensor product over the vectors that describe the states of the qubits making up that register. %, this will be
\begin{Example}
  Let
  \begin{align}
    \ket{\psi} \equiv \begin{pmatrix}\alpha_A\\\beta_A\end{pmatrix}, \ket{\phi} = \begin{pmatrix}\alpha_B\\\beta_B\end{pmatrix}\,,
  \end{align}
  then the tensor product of those two vectors is
  \begin{align}
    \ket{\psi}\otimes\ket{\phi}\equiv
    \begin{pmatrix}
      \alpha_A\\
      \beta_A
    \end{pmatrix}
    \otimes
    \ket{\phi}
    \equiv
    \begin{pmatrix}
      \alpha_A\ket{\phi}\\
      \beta_A\ket{\phi}
    \end{pmatrix}
    \equiv
    \begin{pmatrix}
      \alpha_A\alpha_B\\
      \alpha_A\beta_B\\
      \beta_A\alpha_B\\
      \beta_A\beta_B
    \end{pmatrix}\,.
  \end{align}
\end{Example}
%
For brevity, the tensor symbol, \(\otimes\), is sometimes omitted, and the dirac notation expanded, so
\begin{align}
  \ket{\psi}\otimes\ket{\phi} = \ket{\psi}\ket{\phi} = \ket{\psi,\phi}\,.
\end{align}
%
For any number \(n\) of vectors of the form  \(\ket{x_i}_{\theta}\) with \(x_i\in \{0,1\}\) and \(\theta \in \{+,\times\}\), a shorthand way to describe their tensor product is
\begin{align}
  \ket{x}_{\theta} = \bigotimes_{i = 1}^{n} \ket{x_i}_{\theta}, x\in\{0,1\}^n
\end{align}

\begin{Example}
  \begin{align}
    \ket{01}_{+} = \ket{0}_+\otimes\ket{1}_+
  \end{align}
\end{Example}
\parAfterEnv
With this, the computational basis for multi-qubit systems can be defined.
An \(n\)-qubit system has \(2^n\) computational basis states that span the \(2^n\)-dimensional complex Hilbert space.
The computational basis for this space is
\begin{align}
  \{\ket{x_i}:i\in\{0,\cdots,2^n-1\}\}\,,
\end{align}
where for each of the basis states \(x_i\in\{0,1\}^n\) is the binary representation of \(i\).

\begin{Example}
  Calculating the explicit vector \(\ket{x_i}\) leads to the quite interesting observation, that the \(i\)th computational basis vector has a \(1\) in the \(i\)th place and zeroes everywhere else.
  \begin{align*}
    \ket{x_i}=
    \begin{pmatrix}
      0\tikzmark[midway]{0}\\
      \vdots\\
      0\\
      1\tikzmark{i}\\
      0\\
      \vdots\\
      0
    \end{pmatrix}
  \end{align*}
  \begin{tikzpicture}[remember picture,overlay]
    \draw[black,thin,<-] (pic cs:i) to [in=255,out=0] + (0cm:.5cm) node[anchor=west,text = black] {index i};
    \draw[black,thin,<-] (pic cs:0) to [in=225,out=0] + (0cm:.5cm) node[anchor=west,text = black] {index 0};
  \end{tikzpicture}
\end{Example}
%
The diagonal basis for \(n\) qubits can be defined analogously.

\subsection{Operator functions}\label{subsec:operator-functions}
Functions on operators are defined by matrix functions acting on the spectral decomposition of an operator.
\begin{Definition}[Operator function]
  Let \(A\) be a normal operator and \(f\colon \mathbb{C}\rightarrow \mathbb{C}\) a function.
  With \(A = \sum_{i}\lambda_i \ket{v_i}\bra{v_i}\) being the spectral decomposition of \(A\), the function
  \begin{align}
    f'(A) \equiv \sum_{i}f(\lambda_i) \ket{v_i}\bra{v_i}\,,
  \end{align}
  is the corresponding \emph{operator function} to \(f\).
\end{Definition}
One such operator function is the \emph{trace} function.
\begin{Definition}[Trace]
  Given an operator \(A\), the trace of \(A\) is
  \begin{align}
    \Tr(A) \equiv \sum_{i}A_{ii}
  \end{align}
  The trace function is cyclic, linear, and invariant under similarity transformations.
\end{Definition}
If an operator has the form \(\ket{\psi}\bra{\psi}\), the trace of that operator is
\begin{align}
  \Tr\left(\ket{\psi}\bra{\psi}\right) = \braket{\psi\vert\psi}\,.
\end{align}
Related to the trace function is the \emph{partial trace}.
\begin{Definition}[Partial trace]
  Given two vector spaces, \(A\) and \(B\), then \(\forall \ket{a_1},\ket{a_2}\in A\) and \(\forall\ket{b_1},\ket{b_2}\in B\), the \emph{partial trace} over system \(B\) is the linear function
  \begin{align}
    \Tr_B\left(\ket{a_1}\bra{a_2}\otimes\ket{b_1}\bra{b_2}\right) \equiv \ket{a_1}\bra{a_2}\Tr\left(\ket{b_1}\bra{b_2}\right))\,.
  \end{align}
\end{Definition}
It is also possible to define the square root of an operator.
\begin{Definition}[Square root of an operator]
The square root of a positive operator \(B\) on a complex Hilbert space is the unique positive operator \(A\), such that \(A^2 = B\).
\end{Definition}

%Rectlinear and Diagonals basis stand in a special relationship to each other