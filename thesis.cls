%! suppress = NonMatchingIf
%! suppress = DuplicateDefinition
%! suppress = NonBreakingSpace
%! suppress = PackageNameDoesNotMatchFileName
%! suppress = FileNotFound
%! Author = Frieder Haizmann
%! Date = 01.11.20

\NeedsTeXFormat{LaTeX2e}
\ProvidesPackage{thesis}
\LoadClass[a4paper,11pt,titlepage]{scrbook}

% Packages

\usepackage[dvipsnames]{xcolor}
\RequirePackage{graphicx}
\usepackage[
style=authoryear,
citestyle=authoryear-comp
]{biblatex}

%% Input
\RequirePackage[english]{babel}
\RequirePackage[utf8x]{luainputenc}
\RequirePackage[T1]{fontenc}
\RequirePackage{lmodern}
\RequirePackage{microtype}
\RequirePackage{wallpaper}

\usepackage[pdfusetitle,
  raiselinks=true,
  a4paper,pdftex,bookmarks,
  bookmarks=true,
  bookmarksopenlevel=1,
  bookmarksopen=true,
  bookmarksnumbered=true,
  hyperindex=true,
  plainpages=false,
  pdfpagelabels=true,
  pdfborder={0 0 0.5}]{hyperref}

\RequirePackage{siunitx}
%\usepackage{pdftexcmds}
 %use -shell-escape flag when using minted
\usepackage{amsmath, amssymb}
\RequirePackage{amsfonts}
\usepackage[algo2e, english, ruled, vlined, linesnumbered]{algorithm2e}
%% Own Functions
\SetKwBlock{Alice}{Alice}{end}
\SetKwBlock{Bob}{Bob}{end}
\usepackage[]{cryptocode}
%% Theorems and Proofs
\usepackage[thmmarks,amsmath]{ntheorem}
%%%% Theorem Sytles from Ntheorems Package Documentation
\theoremstyle{break}
\theoremheaderfont{\normalfont\bfseries}\theorembodyfont{\slshape}
\theoremsymbol{\ensuremath{\scriptstyle\diamondsuit}}
\theoremseparator{:}
\newtheorem{Theorem}{Theorem}

\theoremstyle{break}
\theoremsymbol{\ensuremath{\scriptstyle\spadesuit}}
%\theoremindent0.5cm
%\theoremnumbering{greek}
\newtheorem{Lemma}{Lemma}

\theoremindent0cm
\theoremsymbol{\ensuremath{\scriptstyle\heartsuit}}
\theoremnumbering{arabic}
\newtheorem{Corollary}[Theorem]{Corollary}

\theoremstyle{change}
\theorembodyfont{\upshape}
\theoremsymbol{\ensuremath{\ast}}
\theoremseparator{}
\newtheorem{Example}{Example}[chapter]

\theoremstyle{plain}
\theoremsymbol{\ensuremath{\scriptstyle\clubsuit}}
\theoremseparator{.}
%\theoremprework{\bigskip\hrule}
%\theorempostwork{\hrule\bigskip}
\newtheorem{Definition}{Definition}[chapter]

\theoremheaderfont{\textsc{}}\theorembodyfont{\upshape}
\theoremstyle{nonumberplain}
\theoremseparator{:}
\theoremsymbol{\rule{1ex}{1ex}}
\newtheorem{Proof}{Proof}
%%%%%%%
% Math Serif Fonts
\usepackage{mathrsfs}

%% Makes life easier
\RequirePackage{tabularx}
\usepackage{braket}
\usepackage{mathtools}
\DeclarePairedDelimiter\abs{\lvert}{\rvert}%
\DeclarePairedDelimiter\norm{\lVert}{\rVert}%

% Fractions
\usepackage{xfrac}

%% Packages from iks template
%%% cryptoseminar cls
\usepackage{ae}               % Almost european, virtual T1-Font
\usepackage{vmargin}          % Adjust margins in a simple way
\usepackage{fancyhdr}         % Define simple headings
\usepackage[absolute,overlay]{textpos}
%\usepackage{multibib}
%%% Vorlage_seminar tex
\usepackage{latexsym}
\usepackage{amstext}

%% More useful packages
\usepackage{listings}
\usepackage{placeins}
\usepackage{csquotes}
\usepackage{url}
\usepackage{multicol}
%\usepackage{ulem}
%\normalem
\usepackage{textgreek}
\usepackage{booktabs}
\usepackage{tikz}
\usepackage{amssymb}
\usepackage{float}
\usepackage{wrapfig}
\usepackage{wasysym}
\usetikzlibrary{positioning, calc, arrows, fit, decorations.pathreplacing, shapes, shapes.multipart,tikzmark,angles,quotes,quantikz}
\tikzset{every picture/.style={remember picture}}
\usepackage[disable,textsize=tiny,colorinlistoftodos]{luatodonotes}
%\usepackage[fixlanguage]{babelbib}
\usepackage{soul,textcomp}
\usepackage{relsize}
\usepackage{svg}
\usepackage{caption}
\usepackage{subcaption}
\usepackage{blochsphere}
\usepackage[nottoc,numbib]{tocbibind}
%\usepackage[title, titletoc]{appendix}
\usepackage{bookmark}

%Abstract
\makeatletter
\newenvironment{abstract}{%
  \if@twocolumn
  \section*{\abstractname}%
  \else
  \small
  \begin{center}%
  {\bfseries \abstractname\vspace{-.5em}\vspace{\z@}}%
  \end{center}%
  \quotation\noindent
  \fi}
\makeatother


\lstdefinelanguage{QSharp}[Sharp]{C}{
  morekeywords={
    namespace,
    open,
    operation,
    function,
    body,
    adjoint,
    auto,
    controlled,
    let,
    set,
    mutable,
    using,
    borrowing,
    Adjoint,
    Qubit, Unit,
    Zero, One
  },
% Define built-in primitives as second-order keywords.
  morekeywords=[2]{H, CNOT, I, X, Y, Z, Message},
  sensitive=true,
  morecomment=[l]{//}
}

\lstset{
  backgroundcolor=\color{white},   % choose the background color; you must add \usepackage{color} or \usepackage{xcolor}; should come as last argument
  basicstyle=\footnotesize,        % the size of the fonts that are used for the code
  breakatwhitespace=false,         % sets if automatic breaks should only happen at whitespace
  breaklines=true,                 % sets automatic line breaking
  captionpos=b,                    % sets the caption-position to bottom
  commentstyle=\color{OliveGreen},    % comment style
  frame=single,	                   % adds a frame around the code
  keepspaces=true,                 % keeps spaces in text, useful for keeping indentation of code (possibly needs columns=flexible)
  keywordstyle=\color{blue},       % keyword style
  numbers=left,                    % where to put the line-numbers; possible values are (none, left, right)
  numbersep=5pt,                   % how far the line-numbers are from the code
  numberstyle=\tiny\color{gray}, % the style that is used for the line-numbers
  rulecolor=\color{black},         % if not set, the frame-color may be changed on line-breaks within not-black text (e.g. comments (green here))
  showspaces=false,                % show spaces everywhere adding particular underscores; it overrides 'showstringspaces'
  showstringspaces=false,          % underline spaces within strings only
  showtabs=false,                  % show tabs within strings adding particular underscores
  stringstyle=\color{magenta},     % string literal style
  tabsize=2,	                   % sets default tabsize to 2 spaces
  escapeinside={(*}{*)},
  title=\lstname                   % show the filename of files included with \lstinputlisting; also try caption instead of title
}

\DeclareCiteCommand{\citetitle}
{\boolfalse{citetracker}%
\boolfalse{pagetracker}%
\usebibmacro{prenote}}
{\ifciteindex
{\indexfield{indextitle}}
{}%
\printtext[bibhyperref]{\printfield[citetitle]{labeltitle}}}
{\multicitedelim}
{\usebibmacro{postnote}}

% Styling directives from iks template, needed so that titlepages looks how it should

%% -------------------------------
%% |         New commands        |
%% -------------------------------
\newcommand{\changefont}[3]{\fontfamily{#1} \fontseries{#2} \fontshape{#3} \selectfont}
%\renewcommand*{\headfont}{\slshape}
%\newcommand{\captionfont}{}
\newcommand{\chapterheadfont}{}
\newcommand{\blankpage}{
\clearpage{\pagestyle{empty}\cleardoublepage}
}
%% --- End of New Commands ---

%% --------------
%% | Title page |
%% --------------

% Logos

\setlength\wpXoffset{-\hoffset}
\setlength\wpYoffset{-2.5cm}
\titlehead{%
% KIT logo german/english
  \vspace{0.7cm}
  \includegraphics[height=1.8cm]{logos/kitlogo_en_cmyk}
  \hfill
  \includegraphics[height=1.8cm]{logos/LogoKastel}
  \ThisCenterWallPaper{1}{title-background.pdf}
}

%
\setkomafont{title}{\huge\sffamily\bfseries}
\setkomafont{subtitle}{\normalfont\large}

\uppertitleback{Karlsruher Institut für Technologie\\ Fakultät für Informatik\\ Postfach 6980\\ 76128 Karlsruhe}

%% variables for title page
\newcommand{\theinstitute}{{KASTEL – Institute of Information Security and Dependability}}
\newcommand{\thethesistype}{}
\newcommand{\thereviewerone}{}
\newcommand{\thereviewertwo}{}
\newcommand{\theadvisorone}{}
\newcommand{\theadvisortwo}{}
\newcommand{\theeditstart}{}
\newcommand{\theeditend}{}

%% formatting commands for titlepage
\newcommand{\thesistype}[1]{\subtitle{\vskip2em #1 {of}}%
\renewcommand{\thethesistype}{#1}}
\newcommand{\myinstitute}[1]{\renewcommand{\theinstitute}{#1}}
\newcommand{\reviewerone}[1]{\renewcommand{\thereviewerone}{#1}}
\newcommand{\reviewertwo}[1]{\renewcommand{\thereviewertwo}{#1}}
\newcommand{\advisorone}[1]{\renewcommand{\theadvisorone}{#1}}
\newcommand{\advisortwo}[1]{\renewcommand{\theadvisortwo}{#1}}

\newcommand{\editingtime}[2]{%
  \renewcommand{\theeditstart}{#1}%
  \renewcommand{\theeditend}{#2}%
%% do not show the date
  \date{}
}

\newcommand{\settitle}{%
  \publishers{%
    \large
    {at the Department of Informatics}\\
    \theinstitute\\[2em]
    \begin{tabular}{l l}
    {Reviewer}: & \thereviewerone\\
    {Second reviewer}: & \thereviewertwo\\
    {Advisor}: &  \theadvisorone\\
    % if there is no second advisor, do not output this line
    \ifthenelse{\equal{\theadvisortwo}{}}{}{%
        {Second advisor}: & \theadvisortwo\\
    }
    \end{tabular}
    \vskip2em
    \theeditstart{} -- \theeditend
  }
}

%% -------------------------------
%% |      Global Settings       |
%% -------------------------------
\setcounter{secnumdepth}{3} % Numbering also for \subsubsections
\setcounter{tocdepth}{3}    % Register \subsubsections in content directory

\setpapersize{A4}
\setmarginsrb{3cm}{1cm}{3cm}{1cm}{6mm}{7mm}{5mm}{15mm}

% \parindent 0cm	% indent beginning of paragraph
% \parskip 0cm		% no margin between paragraphs

\marginparwidth 0mm
\marginparsep 0mm
\marginparpush 0pt
\columnwidth\textwidth

\setlength{\topsep}{0pt}
\setlength{\partopsep}{5pt plus 4pt minus 2pt}
%% --- End of global Settings ---



%% -------------------------------
%% |          Headings           |
%% -------------------------------
\pagestyle{fancy}
\renewcommand{\theHchapter}{\Roman{chapter}\thechapter}
\renewcommand{\thechapter}{\Roman{chapter}}
%\renewcommand{\chaptermark}[1]{\markboth{\thechapter.\ #1}{\@title}}
\fancyhf{}
\fancyhead[LE,RO]{{\headfont\thepage}}			% Left/right header for even/odd pages
\fancyhead[LO]{\headfont\nouppercase{\rightmark}}	% Header for left page (odd)
\fancyhead[RE]{\headfont\nouppercase{\leftmark}}	% Header for right page (even)
% \fancyfoot[C]{\thepage}
\renewcommand{\headrulewidth}{0.5pt}
\renewcommand{\footrulewidth}{0pt}
\fancypagestyle{plain}{%
\fancyhf{}						% No Header and Footer fields
\renewcommand{\headrulewidth}{0pt}
\renewcommand{\footrulewidth}{0pt}
\fancyfoot[C]{\thepage}
}
%% --- End of Headings ---
