\section{Unconditional Security}\label{sec:unconditional-security}
After the protocol and attack of~\textcite{IEEE1984} was first published, protocols were proposed, falsely claiming to achieve unconditional secure {\qbc}-protocols.
Notable is a protocol by~\textcite{brassardQuantumBitCommitment1993}, which was later called \textsc{BCJL}, as its disproval lead to Mayers' generalized no-go theorem and proof.
However, there has still been a large number of papers that tried to circumvent Mayers' and Lo and Chau's proof without changing the assumptions, for example by combining quantum protocols with classical protocols or by using anonymous states~\autocite{yuenUnconditionallySecureQuantum2005}.
Those protocols were then disproven by~\textcite{darianoReexaminationQuantumBit2007} who published a more general no-go theorem.
\mypar
This does not mean that unconditionally secure {\qbc} is impossible under all assumptions.
By introducing new assumptions like the existence of noise in certain places or by taking special relativity into account, unconditionally secure {\qbc} schemes can be constructed.
Modifying the goal for security has also been considered.

\subsection{Noise and Decoherence}\label{subsec:decoherence}
%When observing quantum systems one is not able to determine the exact amplitude and phase of each qubit measured.
%However these \enquote{hidden variables} still govern how bits interact in a quantum circuit or under an operation.
%The preservation of these properties is called \emph{quantum coherence}.{explain less handy-wavy}{}
%
%Quantum systems are governed by \enquote{hidden variables} meaning
\emph{Decoherence} usually describes the phase-dampening process that causes a particle to lose its quantum properties by interacting with many environmental particles and \emph{quantum-noise} is usually used more broadly to describe the loss of coherence.
However, the two terms have historically been used interchangeably by some authors~\autocite[398]{nielsenQuantumComputationQuantum2010a}.
%[inline]{Explain what decoherence is, phase dampening process, is responsible for losing quantum coherence. It is the reason why we do not observe quantum effects on large scale objects. Also explain the more general term of quantum noise~\autocite[398]{nielsenQuantumComputationQuantum2010a}}
\mypar
Noise is usually an unwanted property in quantum computation as loss of quantum properties is counterproductive when trying to use them in calculations.
As a consequence, quantum error-correction is a field of great interest~\autocite[453]{nielsenQuantumComputationQuantum2010a}.
However, in \emph{quantum cryptography} there have been investigations on whether noise or decoherence can be used to actually make protocols \emph{more} secure, as many attacks, including the Mayers-Lo-Chau attack, rely on cohering quantum properties to be carried out successfully.

\subsubsection{Noisy Storage}\label{subsubsec:noisy-storage}\label{subsubsec:konig}
\textcite{konigUnconditionalSecurityNoisy2012} introduce the concept of a \emph{noisy-storage} channel to formally relate the security of a {\qbc}-protocol to sending information through such a channel.
They assume that no large scale reliable quantum storage is available, therefore they introduce a model in which storage is limited and stored qubits are subjected to a specified level of noise over time.
In that they also assume Markovian noise, as they assume that noise only ever increases and thus information stored only ever decreases.
\mypar
They force participants of a protocol to use the storage device by introducing time delays and then argue that realistic levels of noise rule out even the most general attack.
This model also includes the \emph{bounded quantum-storage} model as initiated by~\textcite{damgardCryptographyBoundedQuantumstorage2005} as a special case.
\mypar
The decoding probability is defined to be the probability that a randomly chosen bit string sent through a storage device can be successfully retrieved.
It is shown that arbitrary channels, that have the property that the decoding probability decays exponentially above a certain threshold, can be used to achieve security.
The authors also show that the number of classical bits, that can be sent through the noisy-storage channel, being limited is a sufficient condition for security.
\mypar
For their {\qbc}-protocol they introduce a novel cryptographic primitive, called \emph{weak string erasure}, on which they base the protocol on.
The authors prove their protocol to be secure in the model they presented and explain that their new primitive could be of independent interest.
\mypar
As the noisy-channel assumption is the only restriction to the adversary, and they claim the noisy-channel assumption to be particular realistic itself, the assumptions needed for this approach appear rather reasonable.

\subsubsection{Trusted Decoherence}\label{subsubsec:trusted-decoherence}\label{subsubsec:dariano}
In addition to generalizing Mayers' proof,~\textcite{darianoReexaminationQuantumBit2007} also use decoherence cleverly to define a decoherence-based {\qbc} scheme, not affected by their own proof.
\mypar
They present three ways to use decoherence for a more secure protocol.
Trusted decoherence in Alice's laboratory is implemented by a notary overseeing Alice's actions during the commitment phase, who is able to take some part of Alice's system and destroy it if cheating is suspected.
The notary is able to leave after the commit phase and thus the authors argue, this protocol is cheaper than one where a notary oversees Alice for the whole protocol.
The constructed protocol is proven to be perfectly concealing and statistically binding.
\mypar
A notary overseeing Alice actions is quite a strong assumption, even it is only for the commit phase, so they also present a protocol based on a weaker assumption.
In this protocol, coherence in Bob's Laboratory is destroyed by a process that Bob has no control over, such that only classical records remain for him.
While this would at first glance weaken the already weaker partner, it is argued if one is able to convince Alice this decoherence is really occurring, she will have lower demands on concealment.
So a protocol, that is both statistically concealing and statistically binding, can be constructed under these assumptions.
In their paper, such a construction is shown and proven to be secure.
\mypar
A third place where trusted decoherence can be used to implement secure bit commitment is the transmission-line between Alice's and Bob's laboratories.
The authors do not show a protocol of their own for this, but rather refer to other implementations that have been shown at the time.
%and since then have been generalized by~\textcite{konigUnconditionalSecurityNoisy2012} as described in.

%The reasoning of~\textcite{konigUnconditionalSecurityNoisy2012} that has been presented in~\autoref{subsubsec:noisy-storage}, why decoherence can be enforced, can likely be adapted here as well. % tough there are differences in the mechanics of the protocols are presented.



\subsubsection{Sender Unable to Perform Large Coherent Measurements}\label{subsubsec:sender-unable-to-perform-(n)-coherent-measurements}\label{subsubsec:salvail}
\textcite{salvailQuantumBitCommitment1998} assumes Alice is not able to perform generalized measurements involving more than \(n\)-qubits.
\mypar
To be more precise~\citeauthor{salvailQuantumBitCommitment1998} first explains, that coherent measurements can be seen as a unitary transformation acting on the observed system and an ancilla, followed by von Neumann measurements in the computational basis, not dissimilar from how measurements are described in Subsection~\ref{subsec:measurement}.
He then argues performing such a \(n\)-coherent measurement for a large \(n\) is difficult, given that a large, coherent, unitary transformation has to be carried out.
A scheme is then presented, for which it is subsequently shown that such large coherent measurements need be performed in order to apply Mayers' attack on it.
So it is proven that the scheme is both statistically binding and statistically concealing under the aforementioned assumption of difficulty, performing large \(n\)-coherent measurements.
%\todo[inline, color=white]{Should there be a further look into the realism of this theory, then Salvails references [17,24,8,31] should be interesting, as they are supposed to show that the assumption about restriction in multi qubit measurements is realistic}
\mypar
The assumption of the impossibility to perform \(n\)-coherent generalized measurements is difficult to assess.
This is because, while at the time of writing such measurements are not possible yet, they could become possible in the future.
~\textcite{blumoffImplementingCharacterizingPrecise2016} demonstrate a highly coherent quantum computing architecture based on superconducting qubits, and use it to establish specific multi-qubit measurements, and~\textcite{kjaergaardSuperconductingQubitsCurrent2020} review this superconducting mode of operation to be promising for larger scale, error-corrected quantum computers.
So, it cannot be ruled out, that with future development, the generalized \(n\)-coherent measurements as described by~\citeauthor{salvailQuantumBitCommitment1998} will in fact be possible.

\subsection{Special Relativity}\label{subsec:special-relativity}
Special relativity and relativistic quantum theory is interesting for cryptography, as its principles allow new models in which tasks are possible that are impossible in purely quantum theoretic models and vice versa.
Those principles include the no-signaling principle which prohibits superluminal signaling (that is sending signals faster than the speed of light) and the principle of information causality which forbids two spacelike separated events to have influence on each other.
The principles of relativistic quantum theory are given by~\textcite{kentQuantumTasksMinkowski2012}, and a great introduction to spacetime and what it means for events to be spacelike separated is given by~\textcite{johnd.nortonEinsteinEveryone2020}.
An example for a task that is impossible in relativistic quantum theory but not in non-relativistic is \emph{summoning}, as given by the no-summoning theorem~\autocite{kentQuantumTasksMinkowski2012}.
\mypar
A cryptographic protocol that makes use of special relativity is called a \emph{relativistic} protocol.
The impossibility of sending signals faster than the speed of light can be used in such a way as to guarantee that communication from one cooperating partner to the other is not possible in less than a fixed amount of time, which in turn can be used instead of the very strong assumption that (one of) the partners is situated in a faraday cage.
\mypar
\label{subsecpart:kentU}
~\textcite{kentUnconditionallySecureBit1999} introduces an unconditionally secure {\bc} protocol based on special relativity.
This is a classical protocol, the enforcement of classicality is suggested to be implemented by use of trusted decohering channels as described in~\ref{subsubsec:trusted-decoherence}.
It works by splitting Alice and Bob each in two cooperating parties and makes it possible for Bob to verify that indeed they cannot communicate with each other.
This is achieved by using a sequence of communications which to maintain security have to be kept up, even after the revelation of the commitment.
\mypar
In the implementation, the locations \(\underbar{x}_1,\underbar{x}_2\) are defined and the Laboratories \(A_i, B_i\) of Alice and Bob have to be within distance \(\delta\) of these locations \(\underbar{x}_i\) for \(i = 1,2\), where \(\triangle{}x = \abs{\underbar{x}_1 - \underbar{x}_2} \gg \delta\).
The test Bob can perform to confirm Alice's locations works as follows: with the speed of light set to \(c = 1\), let \(B_i\) send test signals to \(A_i\) and to pass the test, \(A_i\) has to reply to these messages within \(2\delta\) time.
\mypar
\(A_i\) need to share a random string between them before the protocol starts.
In the protocol, classical bit commitments are carried out in regular intervals, alternating between commitments from \(A_1\) to \(B_1\) and commitments from \(A_2\) to \(B_2\) where each commitment consumes an increasingly long segment of the random string shared by the Alices.
The time in which each of those commitments has to be completed is limited to a fixed interval \(\triangle{}t \gg \triangle{}x\).
This is continued until an Alice chooses to unveil the commitment.
To verify the unveiled bit, Bob has to gather data of both \(B_1\) and \(B_2\) in one place.
It is proven that this need to wait for information, both for unveiler and unveilee, implies that the protocol presented is not vulnerable to Mayers' or Lo and Chau's attack.
It also is not vulnerable against the attack of~\textcite{darianoReexaminationQuantumBit2007}.
\mypar
A quantum version of the protocol where Alice is able to keep a qubit in superposition is also discussed, but it is argued that this gives her no advantage over randomly choosing a classical bit.
However, it is mentioned that while this is true for a standalone {\qbc}-protocol, caution has to be held when using it as a sub-protocol of another protocol.
\mypar
It is also noted that an implementation of this protocol would need an exponential increase in channel capacity for an increasing commitment time, as an ever-increasing amount of information has to be sent in a fixed time interval.
\mypar
Channel capacity describes how much information can be transmitted over a classical channel such as a copper wire, or over a respective quantum channel.
%While it is great as a proof of concept, the exponentially increasing needed channel capacity relative to the duration of the protocol presents a problem.
It is clear that the requirement for an exponential increasing channel capacity makes the protocol not actually practically usable.
The protocol thus is a \emph{theoretical} solution to the problem of unconditionally secure bit commitments over arbitrary long time intervals.
It is argued however, that the time delay enforcement, on which the protocol is based around, could also be implemented using other physical assumptions.
