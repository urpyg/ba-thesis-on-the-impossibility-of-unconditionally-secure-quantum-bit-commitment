\section{Concealing and Binding Security Tradeoffs}\label{sec:concealing-and-binding-security-tradeoffs}
While in most if not all realistic settings unconditionally secure, non-relativistic quantum bit commitment is impossible, protocols fulfilling weaker security conditions are possible, considering some assumptions.
Of particular interest are those protocols that are either unconditionally binding or unconditionally concealing and computationally concealing or computationally binding respectively, as other cryptographic protocols can be based upon {\qbc}-protocols with these properties.
For example zero knowledge arguments can be based on unconditionally concealing bit commitment~\autocite{brassardMinimumDisclosureProofs1988}.
\mypar
Most of the protocols presented in this section are based on the assumption of the existence of either quantum one-way functions, or quantum one-way permutations.
%As in classical cryptography, the security of the computational security definitions presented here are based on assumptions, that certain (quantum) computations are hard to perform.

\subsection{Unconditionally Concealing}\label{subsec:concealing}
\subsubsection{From Quantum One-Way Permutations}\label{subsubsec:dumais}
\textcite{dumaisPerfectlyConcealingQuantum2000} introduce and prove the security of a perfectly concealing and computationally binding {\qbc} scheme based on quantum one-way permutations.
For the perfectly concealing property, it is shown that the commitment states on Bobs side are in fact always identical.
The authors parametrize their binding definition with a performance success ratio, \(R(n) \geq T(n)/S(n)\) where \(T(n)\) is the number of universal gates, needed to implement an adversary, \(S(n)\) the probability they are successfully able to cheat and \(n\) the security parameter.
To be more precise, they define a {\qbc} to be \(R(n)\)-binding if no adversary with  \(R(n) \geq T(n)/S(n)\) exists.
An analogous definition of a \(R(n)\)-secure family of quantum one-way functions is also given.
In their analysis they then show, that for any \(R(n)\)-secure family of quantum one-way permutations their {\qbc}-protocol, that takes such a family as an input, is \(R'(n)\)-binding with \(R'(n)\in \Omega\left(\sqrt{R(n)}\right)\).
%
They also argue, alongside with~\textcite{salvailQuantumBitCommitment1998}, that the limitation keeping a conditional attacker from breaking the scheme is not their computational power, but the size of the entanglement that their quantum computer can deal with.



\subsubsection{From Quantum One-Way Functions}\label{subsubsec:crepeau}
\textcite{crepeauHowConvertFlavor2001} improve this by proving that it is possible to convert a statistically binding (and computationally concealing) {\qbc} scheme to a statistically concealing (and computationally binding) one.
This means that a statistically concealing and computationally binding {\qbc} scheme can be based upon any quantum one-way function.
\mypar
Their proof works as follows: a statistically concealing and computationally binding {\qbc} is constructed from a quantum oblivious transfer protocol and a statistically binding {\qbc}.
For quantum oblivious transfer it is shown that it can be based on a statistically binding bit commitment scheme.
It is proven that the classical statistically binding commitment scheme given by~\textcite{naorBitCommitmentUsing1991}, which is based upon a pseudo-random bit generator, is secure against an adversary with access to a quantum computer if this pseudo-random bit generator is also secure against a quantum adversary.
This can be achieved using a quantum one-way function.
The one-way function is used to construct a pseudo-random bit generator which is resistant to quantum distinguishers.
\mypar
Thus, both statistically binding and computationally concealing {\qbc} and computationally binding and statistically concealing {\qbc} can be constructed from a quantum one-way function.
%
This stands in contrast to the classical case, where a statistically binding and computationally concealing {\bc} can be based upon a one-way function, but computationally binding and statistically concealing {\bc} schemes can be based on one-way permutations, but not on one-way functions.
This means in this instance, the assumptions in the quantum case can be weakened compared to the classical case.

\subsection{Unconditionally Binding}\label{subsec:binding}
\subsubsection{From Quantum One-Way Permutations}\label{subsubsec:adcock}
\textcite{adcockQuantumGoldreichLevinTheorem2002} show a perfectly binding and computational concealing {\qbc}-protocol from any quantum one-way permutation, as a complement to Dumais et al.\ computationally binding protocol.
\mypar
The classical Goldreich-Levin-Theorem reduces inverting a one-way function to predicting a hard-predicate of this function.
~\citeauthor{adcockQuantumGoldreichLevinTheorem2002} present a quantum version of this theorem.
They then use it to construct a perfectly binding, computationally concealing {\qbc}.

\subsubsection{From Approximate-Preimage-Size Quantum One-Way Functions}\label{subsubsec:koshiba}
\textcite{koshibaStatisticallyHidingQuantumBit2009} define the \emph{almost onto} property for quantum one-way functions.
The existence of such quantum one-way functions is an assumption that is stronger than that of quantum one-way functions in general, but weaker than the assumption of quantum one-way permutations.
Such a quantum one-way function is called an \emph{approximate-preimage-size quantum one-way function}.
Based on such approximate-preimage-size quantum one-way function, they define a {\qbc}-protocol that is statistically concealing and computationally binding.
Their {\qbc}-protocol is a generalization of the protocol described in~\ref{subsubsec:dumais}, in which they replace the quantum one-way permutations of~\textcite{dumaisPerfectlyConcealingQuantum2000} with quantum one-way functions.
\mypar
In their proof, they base the computational binding property on the quantum one-way property of the function used by the protocol and the statistically concealing property on its almost-onto property.

\subsubsection{From Quantum One-Way Functions}
As mentioned in~\ref{subsubsec:crepeau},~\textcite{crepeauHowConvertFlavor2001} also show a statistically binding and computationally secure {\qbc} based on Naors classical {\bc}.
While the protocol of~\textcite{crepeauHowConvertFlavor2001} only assumes quantum one way functions and~\textcite{koshibaStatisticallyHidingQuantumBit2009} thus hold stronger assumptions, the advantage of the latter over the former protocol is that it is non-interactive and thus easier to analyze for potential flaws.
~\textcite{koshibaStatisticallyHidingQuantumBit2009} also argue, if there exists a general construction of an almost-onto quantum one-way function, then a statistically concealing quantum bit commitment scheme can be constructed from any quantum one-way function which would, for the reason mentioned above, be an advantage over~\textcite{crepeauHowConvertFlavor2001}.

\subsection{Partially Binding, Partially Concealing}\label{subsec:partially-binding-partially-concealing}
As various protocols with varying degrees of concealment and bindingness have been introduced, the question arises what the best possible tradeoff between those two properties looks like.
\label{subsecpart:spekkens}
~\textcite{spekkensDegreesConcealmentBindingness2001} explore exactly that.
They define measures for bindingness, Alice's control \(0 \leq C \leq \frac{1}{2}\), and concealment, Bob's gain \(0 \leq G \leq \frac{1}{2}\).
They then explore the bounds of those measures on their own and by fixing the one while maximizing the other.
Their result for \(G^{\max}\), the maximum amount of information Bob can gather is given by the trace distance: \(G^{\max} \geq \frac{1}{2} D(\rho_0,\rho_1)\).
In contrast to that, the maximum amount of control Alice has to change her mind, \(C^{\max}\), is restricted by the fidelity:  \(C^{\max} \geq \frac{1}{2}F(\rho_0,\rho_1)^2\).
The states \(\rho_0\) and \(\rho_1\) are the states in Bob's possession after the commit-phase.
They also define the class of so-called \emph{purification bit commitment protocols}, of which BB84 is part of.
It is shown that protocols of that class saturate the bounds \(G^{\max} = \frac{1}{2} D(\rho_0,\rho_1)\) and  \(C^{\max} \geq \frac{1}{2}F(\rho_0,\rho_1)\).
\begin{figure}[htpb]
  \centering
  \input{possible/assumptions-to-security-prameters.tikz}
  \caption{Relation between assumptions, other primitives and different levels of security for {\qbc}}
  \label{fig:assumtions-to-security-parameters}
\end{figure}