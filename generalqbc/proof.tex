%\chapter{Proof of the impossibility Theorem}\label{ch:proof-of-the-impossibility-theorem}
%\section[placerholder]{lorem}\label{sec:ipsum}
%Placeholder
\subsection{Transforming Zero to One}\label{subsec:transforming-zero-to-one}
%Suppose the states \(\ket{\psi_{b,\gamma}'}\) created by a dishonest Alice are so, that after the commit phase, the Fidelity \(F'(\gamma)\) of the reduced density matrices, as defined in Lemma~\ref{lemma:fidelity_close_to_one}, is one.
Suppose the commitment protocol is perfectly secure against Bob.
This means, following Lemma~\ref{lemma:fidelity_close_to_one}, that \(F'(\gamma) = 1\).
So \(\rho_{\mathcal{B}}\left(\ket{\psi^{\A}_{0,\gamma}}\right) = \rho_{\mathcal{B}}\left(\ket{\psi^{\A}_{1,\gamma}}\right) \coloneqq \rho_{\mathcal{B}} \).
%
It is shown that Alice is then able to cheat, by changing her commitment during the holding phase.
Later this will be extended to commitment protocols that are not perfectly, but unconditionally, secure against Bob.
%\(\ket{\psi^{\A}_{b,\gamma}}\) is the corresponding collapsed state of the remaining system \(\H_{\mathcal{A}}\otimes \H_{\mathcal{B}} \otimes {\H}_{E,\mathcal{B}}\).
%\(\rho_{\mathcal{B}}\left(\ket{\psi^{\A}_{0,\gamma}}\right) = \rho_{\mathcal{B}}\left(\ket{\psi^{\A}_{1,\gamma}}\right) = \rho_{\mathcal{B}} = \Tr_{\H_{\mathcal{A}} \otimes{} {\H}_{E,\mathcal{A}}}\left(\ket{\psi^{\A}_{b,\gamma}}\bra{\psi^{\A}_{b,\gamma}}\right)\) is reduced density matrix of \(\H_{\mathcal{B}} \otimes {\H}_{E,\mathcal{B}}\)\\
\mypar
\begin{Definition}[ensembles\footnotemark]\label{def:ensembles}
  An \emph{ensemble} of quantum states is a collection of normalized states \(\ket{\hat{\psi}_1,\cdots,\hat{\psi}_n}\) with fixed a priori probabilities \(p_1,\cdots,p_n\).
To any such ensemble a density matrix is associated with.
\begin{align}
    \rho = \sum_{i=1}^{n} p_i \ket{\hat{\psi}_i}\bra{\hat{\psi}_i}\,.
\end{align}
For convenience, the ensemble is represented as \(\left\{\ket{\psi_i},\cdots,\ket{\psi_n}\right\}\) where \(\ket{\psi_i}= \sqrt{p_i}\ket{\hat\psi_i}\), so that \(p_i = \braket{\psi_i\vert\psi_i}\) and
\begin{align}
    \rho = \sum_{i = 1}^{n} \ket{\psi_i}\bra{\psi_i}
\end{align}
  An \emph{eigen-ensemble} of a given state \(\rho\) on an \(n\)-dimensional Hilbert space with an orthonormal basis of eigenvectors \(\hat{e}_{1},\ldots,\hat{e}_{n}\) that have the eigenvalues \(\lambda_1,\ldots,\lambda_n\), is an ensemble of the form \(\left\{\ket{e_1},\ldots,\ket{e_k}\right\}\) where \(\braket{e_i\vert{}e_i} = \lambda_i\) for each \(i\).
  States \(\ket{e_1}\) where the eigenvalues \(\lambda_i = 0 \) are not included in the ensemble, thus \(k = \text{rank}(\rho) \leq n\).
\end{Definition}
\footnotetext{Following~\textcite[section 1]{HUGHSTON199314}.}
\begin{Theorem}[Ensembles of a density matrix\footnotemark]\label{thm:ensembles}
  For a given density matrix \(\rho\) on an \(n\)-dimensional Hilbert space, let \(\ket{\phi_1},\ldots,\ket{\phi_n}\) be any ensemble of pure states with associated density matrix \(\rho\).
  Then there exists a matrix \(N \in \mathbb{C}^{s\times k}\) whose columns are k orthonormal vectors in \(\mathbb{C}^r\), so \(r \geq k \coloneqq \text{rank}(\rho)\), such that
  \begin{align}
      \ket{\phi_i} = \sum_{j=1}^{k}N_{ij}\ket{e_j}, i = 1,\ldots,s \,.
  \end{align}
\end{Theorem}
\footnotetext{Following~\textcite[section 2]{HUGHSTON199314}.}
%\todo[color=white, caption={Mention LoChau here?}]{This theorem is actually a corollary. It follows from the proof that all purifications of a fixed density matrix are related to another by unitary transformation acting on A alone~\cite[3]{loQuantumBitCommitment1997}}{}
\begin{Theorem}[Zero to One]\label{thm:zero-to-one}
  \(\ket{\psi^{\A}_{b,\gamma}}\) is the corresponding collapsed state of the remaining system \(\H_{\mathcal{A}}\otimes \H_{\mathcal{B}} \otimes {\H}_{E,\mathcal{B}}\).
  \(\rho_\mathcal{B}\left(\ket{\psi^{\A}_{0,\gamma}}\right) = \rho_\mathcal{B}\left(\ket{\psi^{\A}_{1,\gamma}}\right)\) implies that there exists a unitary transformation on Alice's side which maps \(\ket{\psi^{\A}_{0,\gamma}}\) into \(\ket{\psi^{\A}_{1,\gamma}}\).
\end{Theorem}
%! suppress = EscapeUnderscore
\begin{Proof}
  \newcommand{\eigenAZero}{\ensuremath{\ket{\hat{a}_i^{(0)}}^{\H_{\A}}}}
  \newcommand{\eigenAOne}{\ensuremath{\ket{\hat{a}_i^{(1)}}^{\H_{\A}}}}
  \newcommand{\eigenBZero}{\ensuremath{\ket{\hat{b}_i^{(0)}}^{\H_{\B}\otimes{}\H_{E,\B}}}}
  \newcommand{\eigenBOne}{\ensuremath{\ket{\hat{b}_i^{(1)}}^{\H_{\B}\otimes{}\H_{E,\B}}}}
  \newcommand{\eigenB}{\ensuremath{\ket{\hat{b}_i}^{\H_{\B}\otimes{}\H_{E,\B}}}}
  \(\rho_{\mathcal{B}}\left(\ket{\psi^{\A}_{0,\gamma}}\right) = \rho_{\mathcal{B}}\left(\ket{\psi^{\A}_{1,\gamma}}\right) \eqqcolon \rho_{\mathcal{B}} = \Tr_{\H_{\mathcal{A}} \otimes{} {\H}_{E,\mathcal{A}}}\left(\ket{\psi^{\A}_{b,\gamma}}\bra{\psi^{\A}_{b,\gamma}}\right)\) is the reduced density matrix of Bob's systems \(\H_{\mathcal{B}} \otimes {\H}_{E,\mathcal{B}}\).
  According to~\hyperref[thm:schmidt]{Schmidt's decomposition theorem}, \(\ket{\psi^{\A}_{0,\gamma}}\) can be written as
  \begin{align}
    \ket{\psi^{\A}_{0,\gamma}} = \sum_{i = 1}^{s}\sqrt{\lambda_{0,i}}\eigenAZero\otimes{}\eigenBZero\,,
  \end{align}
  where \(\eigenAZero\) are eigenstates of \(\rho_A\left( \ket{\psi^{\A}_{0,\gamma}}\right)\) forming an orthonormal basis of \(\H_{\mathcal{A}}\) and \(\eigenBZero\) are eigenstates of \(\rho_B\) forming an orthonormal basis for system \({\H}_{E,\mathcal{B}}\).
  \(\lambda_{0,i}\) are the eigenvalues of \(\Tr_{\H_\mathcal{A} \otimes{} {\H}_{E,\mathcal{A}}}(\ket{\psi^{\A}_{0,\gamma}}\bra{\psi^{\A}_{0,\gamma}})\)  and of \(Tr_{\H_{\mathcal{B}} \otimes{} {\H}_{E,\mathcal{B}}}(\ket{\psi^{\A}_{0,\gamma}}\bra{\psi^{\A}_{0,\gamma}})\) with the same multiplicity where any extra dimensions are made up with zero eigenvalues~\autocite{HUGHSTON199314}.
  This means that
  \begin{align}
    \rho_{\mathcal{B}} = \sum_{i= 1}^{s}\lambda_{0,i}\eigenBZero\bra{\hat{b}_i^{(0)}}^{{\H}_{\B}\otimes{} {\H}_{E,\B}}\,,
  \end{align}
  and thus
  \begin{align}
    \left\{\sqrt{\lambda_{0,i}}\eigenBZero\right\}
  \end{align}
  is a \(\rho_\mathcal{B}\) eigen-ensemble, as
  \begin{align}
    \braket{\hat{b}^{(0)}\sqrt{\lambda_{0,i}}\vert\sqrt{\lambda_{0,i}}\hat{b}^{(0)}} \stackrel{\ket{\hat{b}_i^{(0)}}\text{ is normal}}{=} \sqrt{\lambda_{0,i}}\sqrt{\lambda_{0,i}} = \lambda_{0,i}\,.
  \end{align}
  %it is easily verifiable that \(\braket{\hat{\imath}_{{\H}_{\mathcal{B}\otimes{} {\H}_{E,\mathcal{B}}}}^{(0)}\sqrt{\lambda_{0,i}}|\sqrt{\lambda_{0,i}}\hat{\imath}_{{\H}_{\mathcal{B}\otimes{} {\H}_{E,\mathcal{B}}}}^{(0)}} = \lambda_{0,i}\).\\
%
%  Define \(k\) to be the rank of \(\rho_{\mathcal{B}}\).
%  Following theorem~\ref{thm:ensembles} there exists a Matrix \(N_{ij}\in\mathbb{C}^{s\times{}k}\) with k-orthonormal columns in \(\mathbb{C}^s\), such that
%  \begin{align}\label{eq:omicron_eigenvalue}
%  \sqrt{\lambda_{0,i}}\{\ket{\hat{\imath}_{{\H}_{B\otimes{} {\H}_{E,\mathcal{B}}}}^{(0)}}\} = \sum_{j= 1}^{k}N_{ij}\ket{e_j}, j =  1,\ldots,s\,,
%  \end{align}
%  where \(\ket{e_j}\) is an eigen-ensemble of \(\rho_{\mathcal{B}}\).\\
  Similarly, since \(\ket{\psi^{\A}_{1,\gamma}}\) is a pure state it can be written as a decomposition of eigenstates and eigenvalues.
  \begin{align}
    \ket{\psi^{\A}_{1,\gamma}} = \sum_{i = 1}^{s}\sqrt{\lambda_{1,i}}\eigenAOne\otimes{}\eigenBOne\,,
  \end{align}
  so \(\left\{\sqrt{\lambda_{1,i}}\eigenBOne\right\}\) is an eigen-ensemble of \(\rho_{\mathcal{B}}\).
%  There exists a Matrix \(M\in\mathbb{C}^{s\times{}k}\) with k-orthonormal columns in \(\mathbb{C}^s\), such that
%  \begin{align}\label{eq:iota_eigenvalue}
%  \sqrt{\lambda_{1,i}}\ket{\hat{\imath}_{{\H}_{\mathcal{B}\otimes{} {\H}_{E,\mathcal{B}}}}^{(1)}} = \sum_{j= 1}^{k}M_{ij}\ket{e_j}, j =  1,\ldots,s\,.
%  \end{align}
  As eigen-ensembles are unique, it follows that \(\lambda_{0,i} = \lambda_{1,i} \eqqcolon \lambda_i\) are the same eigenvalues and
  \begin{align}
    \eigenBZero = \eigenBOne {\eqqcolon} \eigenB
  \end{align}
  are the same eigenstates.
%  With~\ref{eq:omicron_eigenvalue} and~\ref{eq:iota_eigenvalue} it follows that
%  \begin{align}
%    (NM^{-1})_i\sqrt{\lambda_{0,i}}\eigenBZero = \sqrt{\lambda_{1,i}}\ket{\hat{\imath}_{{\H}_{\mathcal{B}\otimes{} {\H}_{E,\mathcal{B}}}}^{(1)}}\,.
%  \end{align}
  \mypar
  There exists a unitary matrix that transforms one orthonormal basis of \(\H_\A\) into another orthonormal basis of \(\H_\A\).
  Let \(S\) be the matrix that transforms \(\eigenAZero\) into \(\eigenAOne\), so
  \begin{align}
    S\eigenAZero = \eigenAOne\,.
  \end{align}
  The same matrix can be used to transform \(\ket{\psi^{\A}_{0,\gamma}}\) into \(\ket{\psi^{\A}_{1,\gamma}}\), as is shown below.
  \begin{align}
    \ket{\psi^{\A}_{1,\gamma}} &=  \sum_{i = 1}^{s}\lambda_{i}\eigenAOne\otimes{}\eigenB\\
    &=  \sum_{i = 1}^{s}\lambda_{i}S\eigenAZero\eigenB\\
    &=  S \sum_{i = 1}^{s}\lambda_{i}\eigenAZero\otimes{}\eigenB\\
    &= S\ket{\psi^{\A}_{0,\gamma}}\,.
  \end{align}
\end{Proof}
\begin{Theorem}[Perfect security against Bob implies insecurity against Alice]\label{thm:no_perfect_security}
  Any {\qbc}-protocol that is perfectly concealing is not unconditionally binding.
\end{Theorem}
\begin{Proof}
  Following Lemma~\ref{lemma:fidelity_close_to_one}, \(F'(\gamma) = 1\), thus \(\rho_\mathcal{B}\left(\ket{\psi^{\A}_{0,\gamma}}\right) = \rho_\mathcal{B}\left(\ket{\psi^{\A}_{1,\gamma}}\right)\).
  Then Theorem~\ref{thm:zero-to-one} asserts that there exists a unitary transformation on Alice's side which maps \(\ket{\psi^{\A}_{0,\gamma}}\) into \(\ket{\psi^{\A}_{1,\gamma}}\).
  This means without loss of generality, Alice can cheat by choosing \(b = 0\), executing \(\commit^{\A}\), and applying the transformation \(S\) in \(\unveil^{\A}\)\footnote{Or at any point in the holding phase.} if she wishes to change her mind.
  Further in \(\unveil^{\A}\) she sends the states to the environment, that she was supposed to send to the environment in \(\commit\) and continues with honest \(\unveil\) after that.
\end{Proof}
\begin{Example}[Modified BB84]
  In Subsection~\ref{subsec:defeating-bb84} it was shown that in BB84, \(\ket{\psi^{\A}_{0,\gamma}} =\ket{\psi^{\A}_{1,\gamma}}\).
  This example modifies BB84 to demonstrate application of~\hyperref[thm:zero-to-one]{theorem zero to one}.
  Consider the new basis that arises when rotating the states of the rectilinear basis on the bloch sphere by \(45^{\circ}\) around the \(y\)-axis and then \(135^{\circ}\) around the \(z\)-axis.
  Its states are
  \begin{alignat}{7}
    &\ket{0}_{\wr} \coloneqq &&\ket{\rightthreetimes} &&\coloneqq \cos\left(\frac{\pi}{8}\right)&&\ket{0}&& + e^{i\frac{\pi}{4}}&&\sin\left(\frac{\pi}{8}\right)&&\ket{1}\\
    &\ket{1}_{\wr} \coloneqq &&\ket{\leftthreetimes} &&\coloneqq \sin\left(\frac{\pi}{8}\right)&&\ket{0}&& + e^{i\frac{5\pi}{4}}&&\cos\left(\frac{\pi}{8}\right)&&\ket{1}\,.
  \end{alignat}
  \textsc{BB84} is now modified as follows: wherever the original protocol uses the diagonal basis, the new protocol uses the new basis.
  This means the cheating states of the new protocol are
  \begin{alignat}{4}
    \ket{\psi^{\A}_{0,\gamma}} &= \frac{1}{\sqrt{2}}&&\ket{00}_{+} + \frac{1}{\sqrt{2}}&&\ket{11}_{+}\\
    \ket{\psi^{\A}_{1,\gamma}} &= \frac{1}{\sqrt{2}}&&\ket{00}_{\wr} + \frac{1}{\sqrt{2}}&&\ket{11}_{\wr}\,.
  \end{alignat}
  \(\ket{\psi^{\A}_{1,\gamma}}\) can be simplified as follows:
  \begin{alignat}{3}
    \ket{\psi^{\A}_{1,\gamma}} =&\frac{1}{\sqrt{2}}\ket{00}_{\wr}& + &\frac{1}{\sqrt{2}}\ket{11}_{\wr}\\
    \equiv& \frac{1}{\sqrt{2}}
    \begin{pmatrix*}\cos(\frac{\pi}{8})\\\sqrt{i}\sin(\frac{\pi}{8})\end{pmatrix*}\otimes\begin{pmatrix*}\cos(\frac{\pi}{8})\\\sqrt{i}\sin(\frac{\pi}{8})\end{pmatrix*}&
    + &\frac{1}{\sqrt{2}}\begin{pmatrix*}\sin(\frac{\pi}{8})\\-\sqrt{i}\cos(\frac{\pi}{8})\end{pmatrix*}\otimes\begin{pmatrix*}\sin(\frac{\pi}{8})\\-\sqrt{i}\cos(\frac{\pi}{8})\end{pmatrix*}\\
    =& \frac{1}{\sqrt{2}}
    \begin{pmatrix*}\cos^2(\frac{\pi}{8})\\\sqrt{i}\cos(\frac{\pi}{8})\sin(\frac{\pi}{8})\\\sqrt{i}\cos(\frac{\pi}{8})\sin(\frac{\pi}{8})\\i\sin^2(\frac{\pi}{8})\end{pmatrix*}&
    + &\frac{1}{\sqrt{2}}\begin{pmatrix*}\sin^2(\frac{\pi}{8})\\-\sqrt{i}\cos(\frac{\pi}{8})\sin(\frac{\pi}{8})\\-\sqrt{i}\cos(\frac{\pi}{8})\sin(\frac{\pi}{8})\\i\cos^2(\frac{\pi}{8})\end{pmatrix*}
    = \frac{1}{\sqrt{2}}\begin{pmatrix*}1\\0\\0\\i\end{pmatrix*}\\
    \equiv& \frac{1}{\sqrt{2}}\ket{00}_{+}+\frac{1}{\sqrt{2}}i\ket{11}_{+}\,.&&
  \end{alignat}
  %For brevity in this example, \(\A\) and \(\B\) will be indicating the systems \(\H_\A\otimes{}\H_{E,\A}\) and \(\H_\B\otimes{}\H_{E,\B}\).
  The reduced density operators on Bob's side are
  \begin{align}
    \rho_{\B}&\left(\ket{\psi^{\A}_{1,\gamma}}\right) = \Tr_{\A}\left(\ket{\psi^{\A}_{1,\gamma}}\bra{\psi^{\A}_{1,\gamma}}\right)\label{eq:rho-b-1-trace}\\
    =& \Tr_{\A}\left(\left(\frac{1}{\sqrt{2}}\ket{00}+\frac{i}{\sqrt{2}}\ket{11}\right)\left(\frac{1}{\sqrt{2}}\bra{00}+\frac{-i}{\sqrt{2}}\bra{11}\right)\right)\nonumber\\
    =& \Tr_A\left(\quad\left(\frac{1}{\sqrt{2}}\ket{0}^{\H_{\A}}\otimes\ket{0}^{\H_{\B}}\right)
    \left(\frac{1}{\sqrt{2}}\bra{0}^{\H_{\A}}\otimes\bra{0}^{\H_{\B}}\right)\right.\nonumber\\
    &\ \qquad+ \left(\frac{1}{\sqrt{2}}\ket{0}^{\H_{\A}}\otimes\ket{0}^{\H_{\B}}\right)
    \left(\frac{-i}{\sqrt{2}}\bra{1}^{\H_{\A}}\otimes\bra{1}^{\H_{\B}}\right)\nonumber\\
    &\ \qquad+ \left(\frac{i}{\sqrt{2}}\ket{1}^{\H_{\A}}\otimes\ket{1}^{\H_{\B}}\right)
    \left(\frac{1}{\sqrt{2}}\bra{0}^{\H_{\A}}\otimes\bra{0}^{\H_{\B}}\right)\nonumber\\
    &\ \qquad+ \left.\left(\frac{1}{\sqrt{2}}\ket{1}^{\H_{\A}}\otimes\ket{1}^{\H_{\B}}\right)
    \left(\frac{1}{\sqrt{2}}\bra{1}^{\H_{\A}}\otimes\bra{1}^{\H_{\B}}\right)\right)\nonumber\\
    =& \frac{1}{2}\Tr_{\A}\left(\ket{0}^{\H_{\A}}\bra{0}^{\H_{\A}}\otimes\ket{0}^{\H_{\B}}\bra{0}^{\H_{\B}}\right)
    + \frac{-i}{2}\Tr_{\A}\left(\ket{0}^{\H_{\A}}\bra{1}^{\H_{\A}}\otimes\ket{0}^{\H_{\B}}\bra{1}^{\H_{\B}}\right)\nonumber\\
    &+ \frac{i}{2}\Tr_{\A}\left(\ket{1}^{\H_{\A}}\bra{0}^{\H_{\A}}\otimes\ket{1}^{\H_{\B}}\bra{0}^{\H_{\B}}\right)
    + \frac{1}{2}\Tr_{\A}\left(\ket{1}^{\H_{\A}}\bra{1}^{\H_{\A}}\otimes\ket{1}^{\H_{\B}}\bra{1}^{\H_{\B}}\right)\nonumber\\
    =& \frac{1}{2}\braket{0\vert{}0}\ket{0}\bra{0}^{\H_{\B}}
    + \frac{-i}{2}\overbrace{\braket{0\vert{}1}}^{= 0}\ket{0}\bra{1}^{\H_{\B}}
    + \frac{i}{2}\overbrace{\braket{1\vert{}0}}^{= 0}\ket{1}\bra{0}^{\H_{\B}}
    + \frac{1}{2}\braket{1\vert{}1}\ket{1}\bra{1}^{\H_{\B}}\nonumber\\
    =& \frac{1}{2}\ket{0}\bra{0}^{\H_{\B}}+\frac{1}{2}\ket{1}\bra{1}^{\H_{\B}}\label{eq:rho-b-result}\nonumber\\
    =& \frac{1}{2}\braket{0\vert{}0}\ket{0}\bra{0}^{\H_{\B}}
    + \frac{1}{2}\overbrace{\braket{0\vert{}1}}^{= 0}\ket{0}\bra{1}^{\H_{\B}}
    + \frac{1}{2}\overbrace{\braket{1\vert{}0}}^{= 0}\ket{1}\bra{0}^{\H_{\B}}
    + \frac{1}{2}\braket{1\vert{}1}\ket{1}\bra{1}^{\H_{\B}}\nonumber\\
    =& \frac{1}{2}\Tr_{\A}\left(\ket{0}^{\H_{\A}}\bra{0}^{\H_{\A}}\otimes\ket{0}^{\H_{\B}}\bra{0}^{\H_{\B}}\right)
    + \frac{1}{2}\Tr_{\A}\left(\ket{0}^{\H_{\A}}\bra{1}^{\H_{\A}}\otimes\ket{0}^{\H_{\B}}\bra{1}^{\H_{\B}}\right)\nonumber\\
    &+ \frac{1}{2}\Tr_{\A}\left(\ket{1}^{\H_{\A}}\bra{0}^{\H_{\A}}\otimes\ket{1}^{\H_{\B}}\bra{0}^{\H_{\B}}\right)
    + \frac{1}{2}\Tr_{\A}\left(\ket{1}^{\H_{\A}}\bra{1}^{\H_{\A}}\otimes\ket{1}^{\H_{\B}}\bra{1}^{\H_{\B}}\right)\nonumber\\
    =& \Tr_{\A}\left(\left(\frac{1}{\sqrt{2}}\ket{00}+\frac{1}{\sqrt{2}}\ket{11}\right)\left(\frac{1}{\sqrt{2}}\bra{00}+\frac{1}{\sqrt{2}}\bra{11}\right)\right)\nonumber\\
    =& \Tr_{\A}\left(\ket{\psi^{\A}_{0,\gamma}}\bra{\psi^{\A}_{0,\gamma}}\right) = \rho_{\B}\left(\ket{\psi^{\A}_{0,\gamma}}\right)\,.
  \end{align}
  Thus, the conditions of theorem~\hyperref[thm:zero-to-one]{zero to one} are met.
  \mypar
  Define \(\rho_{\B} \coloneqq \rho_{\B} \left(\ket{\psi^{\A}_{1,\gamma}}\right)\).
  Analogous to Equations~\ref{eq:rho-b-1-trace}--\ref{eq:rho-b-result},
  \begin{align}
    \rho_{\A}\left(\ket{\psi^{\A}_{0,\gamma}}\right) = \rho_{\A}\left(\ket{\psi^{\A}_{1,\gamma}}\right) = \frac{1}{2}\ket{0}\bra{0}^{\H_{\A}}+\frac{1}{2}\ket{1}\bra{1}^{\H_{\A}} = \frac{1}{2}\begin{pmatrix*}1&\\&1\end{pmatrix*}\,.
  \end{align}
  So \(\rho_{\B},  \rho_{\A}\left(\ket{\psi^{\A}_{0,\gamma}}\right)\) and \(\rho_{\A}\left(\ket{\psi^{\A}_{1,\gamma}}\right)\) have the eigenvalues \(\lambda_{1} = \lambda_{2} = \frac{1}{2}\).
  \begin{align}
      \ket{f_1} \coloneqq \ket{0}, \ket{f_2} \coloneqq \ket{1}
  \end{align}
  are eigenvectors of \(\rho_B\),
  \begin{align}
      \ket{e^{(0)}_1} \coloneqq \ket{0},\ket{e^{(0)}_2} \coloneqq \ket{1}
  \end{align}
  eigenvectors of \(\rho_{\A}\left(\ket{\psi^{\A}_{0,\gamma}}\right)\) and
  \begin{align}
      \ket{e^{(1)}_1} \coloneqq \ket{0},\ket{e^{(1)}_2} \coloneqq i\ket{1}
  \end{align}
  eigenvectors of \(\rho_{\A}\left(\ket{\psi^{\A}_{1,\gamma}}\right)\).
  With that, Schmidt decompositions of \(\ket{\psi^{\A}_{0,\gamma}}\) and \(\ket{\psi^{\A}_{1,\gamma}}\) are
  \begin{align}
    \ket{\psi^{\A}_{b,\gamma}} = \sum_{i = 1}^{2} \sqrt{\lambda_{i}}\ket{e^{(b)}_i}\ket{f_i},\quad b \in \{0,1\}\,.
  \end{align}
  To find \(S\) the following equations are solved.
  \begin{alignat}{3}
    S\ket{0} &= \ket{0} \Rightarrow S &= \begin{pmatrix*}1&\\&*\end{pmatrix*}\\
    S\ket{1} &= i\ket{1} \Rightarrow S &= \begin{pmatrix*}*&\\&i\end{pmatrix*}\,.
  \end{alignat}
  It follows, that
  \begin{align}
      S = \begin{pmatrix*}1&\\&i\end{pmatrix*}
  \end{align}
  is the operation Alice has to apply only on her side to convert \(\ket{\psi^{\A}_{0,\gamma}}\) to \(\ket{\psi^{\A}_{1,\gamma}}\).
\end{Example}

\subsection{Non-Perfect QBC}\label{subsec:non-perfect-cheat}
It will now be shown, that such a transformation not only exists if the {\qbc}-protocol is perfectly concealing, but also if it is unconditionally concealing.
Thus, it will be shown a cheating transformation on Alice's side exists if \(F'(\gamma)\) is not equal, but arbitrarily close to one.

\begin{Definition}[Purification\footnotemark]\label{def:purification}
Let \(\rho\) be any mixed state on Hilbert space \(\H_1\).
  A \emph{purification} of \(\rho\) is any pure state \(\ket{\phi}\) in any extended hilbert space \(\H_1\otimes{}\H_2\) with the property that \(\rho = \Tr_{\H_2}(\ket{\phi}\bra{\phi})\).
\end{Definition}
\footnotetext{Following~\textcite[Definition 1]{jozsaFidelityMixedQuantum1994}.}
\begin{Theorem}[Uhlmann's theorem\footnotemark]\label{thm:uhlmann}
Let \(\rho_1, \rho_2\) be states of the same quantum system \(\H_1\) and \(\ket{\phi_1}, \ket{\phi_2}\) purifications of \(\rho_1\) and \(\rho_2\) respectively into \(\H_1\otimes{}\H_2\).
  Then the fidelity between \(\rho_1, \rho_2\) is the maximization over the inner product of all such purifications.
\begin{align}
  F(\rho_1,\rho_2) = \max_{\ket{\phi_1}, \ket{\phi_2}}{\abs{\braket{\phi_1\vert\phi_2}}}
\end{align}
%\todo[inline, color=white, caption={Mention different definitions for fidelity?}]{Mention different definitions for fidelity? Fidelity can be defined with~\autocite{jozsaFidelityMixedQuantum1994} or without~\autocite[Theorem~9.4]{nielsenQuantumComputationQuantum2010a}, square around the whole term. Uhlmann's theorem then also includes a square or not.}
\end{Theorem}
\footnotetext{Following~\textcite[Theorem 2]{jozsaFidelityMixedQuantum1994}.}
For readability the following short labels for states and density operators will be used:
\begin{align}
  \rho_{\mathcal{B}}\left(\ket{\psi_{0,\gamma}^{\A}}\right) \eqqcolon \rho_0,\quad\rho_{\mathcal{B}}\left(\ket{\psi_{1,\gamma}^{\A}}\right) \eqqcolon \rho_1,\quad\ket{\psi_{0,\gamma}} \eqqcolon \ket{\psi_0},\quad\ket{\psi_{1,\gamma}^{\A}} \eqqcolon \ket{\psi_1}\,.
\end{align}
%
\begin{Theorem}[Unconditional security against Bob implies insecurity against Alice]\label{thm:no_unconditional_security}
  Any {\qbc}-protocol that is unconditionally concealing is not unconditionally binding.
\end{Theorem}
\begin{Proof}
Following Lemma~\ref{lemma:fidelity_close_to_one}, \(0 < F'(\gamma) = F(\rho_0,\rho_1) = 1 - \delta\), with \(\delta > 0\), for a very small delta.
%
Per Definition~\ref{def:purification}, \(\ket{\psi_{1}}\) is a purification of \(\rho_1\).
 As~\hyperref[thm:uhlmann]{Uhlmann's theorem} applied to \(\rho_0\) and \(\rho_1\) describes a maximization over all purifications of those states, there exists a purification \(\ket{\psi_{01}}\) of \(\rho_0\), such that
\begin{align}\label{eq:psi01andpsi1}
  \braket{{\psi_{01}}\vert{\psi_{1}}} \geq F'(\gamma)\,.
\end{align}
Since \(\ket{\psi_0}\) and \(\ket{\psi_{01}}\) are purifications of the same reduced density operator \(\rho_0\), there exists an operation on \({\H}_\mathcal{A}\) that transforms \(\ket{\psi_0}\) into \(\ket{\psi_{01}}\).
%
Equation~\ref{eq:psi01andpsi1} implies, that the probability of Bob being able to differentiate \(\ket{\psi_1}\) from \(\ket{\psi_{01}}\) and thus detect Alice's cheat, goes to zero as \(\delta\) goes to zero and \(F'(\gamma)\) nears one.
This is underlined in the following equation.
\begin{align}
  F(\ket{\psi_{01}}\bra{\psi_{01}}, \ket{\psi_{1}}\bra{\psi_{1}}) \geq 1 - \delta\,.
\end{align}
% in other word probability of unveil(psi_01) = 1 increases
\end{Proof}

\begin{Theorem}[Unconditionally secure {\qbc} is impossible]
  No quantum bit commitment that falls under the framework presented in this chapter is unconditionally secure.
\end{Theorem}
\begin{Proof}
  A {\qbc}-protocol that is not unconditionally hiding is not unconditionally secure.
  Assume the protocol is unconditionally hiding.
  Then following Theorems~\ref{thm:no_perfect_security} and~\ref{thm:no_unconditional_security} it is not unconditionally binding.
\end{Proof}

\subsection{Complexity of Alice's Cheat}\label{subsec:complexity-of-alice's-cheat}
A malicious Alice has been modeled as an unconditional attacker.
In this section it will be shown how much computational power Alice would actually need to perform the attack, by showing the computational complexity of the attack to be in \(\mathcal{O}(2^{n\omega}),\quad 2 < \omega < 2.37369\).
\subsubsection{Complexity of the Transformation}\label{subsubsec:complexity-of-the-transformation}
As seen in Subsection~\ref{subsec:transforming-zero-to-one}, to find the cheating transformation that maps a state committing to zero to a state committing to one, an eigen-decomposition of the reduced density operator on Bobs side has to be calculated and an appropriate basis transformation has to be found.
Transforming one basis into another in high dimensions is a costly procedure.
This is shown in an example and then generalized.
\begin{Example}
  The Hilbert Space for a two qubit system is of dimension \(2^2 = 4\), thus a basis consists of four, four-dimensional state vectors.
  Two bases of this Hilbert Space are considered:
  \begin{align}
      b = \left\{\mathbf{b}_{1},\mathbf{b}_{2},\mathbf{b}_{3},\mathbf{b}_{4}\right\}
  \end{align}
  and
  \begin{align}
      b' = \left\{\mathbf{b}'_{1},\mathbf{b}'_{2},\mathbf{b}'_{3},\mathbf{b}'_{4}\right\}\,,
  \end{align}
  where for \(i = 1,2,3,4\)
  \begin{align}
      \mathbf{b}_i = \begin{pmatrix}\alpha_i\\\beta_i\\\gamma_i\\\delta_i\end{pmatrix}
  \end{align} and
  \begin{align}
      \mathbf{b}'_i = \begin{pmatrix}\alpha'_i\\\beta'_i\\\gamma'_i\\\delta'_i\end{pmatrix}
  \end{align} respectively.
  An operation S is searched, such that it transforms states of \(b\) to states of \(b'\).
  Thus, such an operator has to fulfill the following system of equations
  \begin{align}
    S  \begin{pmatrix}\alpha_i\\\beta_i\\\gamma_i\\\delta_i\end{pmatrix} =  \begin{pmatrix}\alpha'_i\\\beta'_i\\\gamma'_i\\\delta'_i\end{pmatrix},\quad i = 1,2,3,4\,.
  \end{align}
  Written differently
  \begin{equation}\label{eq:equation}
  S
  \overbrace{
    \begin{pmatrix}\alpha_1&\beta_1&\gamma_1&\delta_1\\
    \alpha_2&\beta_2&\gamma_2&\delta_2\\
    \alpha_3&\beta_3&\gamma_3&\delta_3\\
    \alpha_4&\beta_4&\gamma_4&\delta_4
    \end{pmatrix}
  }^{=(\mathbf{b}_{1},\mathbf{b}_{2},\mathbf{b}_{3},\mathbf{b}_{4})}
  =
  \overbrace{
    \begin{pmatrix}\alpha'_1&\beta'_1&\gamma'_1&\delta'_1\\
    \alpha'_2&\beta'_2&\gamma'_2&\delta'_2\\
    \alpha'_3&\beta'_3&\gamma'_3&\delta'_3\\
    \alpha'_4&\beta'_4&\gamma'_4&\delta'_4\end{pmatrix}
  }^{=(\mathbf{b}'_{1},\mathbf{b}'_{2},\mathbf{b}'_{3},\mathbf{b}'_{4})}\,,
  \end{equation}
  and since \((\mathbf{b}_{1},\mathbf{b}_{2},\mathbf{b}_{3},\mathbf{b}_{4})\) is orthonormal,
  \begin{align}
    S = (\mathbf{b}'_{1},\mathbf{b}'_{2},\mathbf{b}'_{3},\mathbf{b}'_{4})(\mathbf{b}_{1},\mathbf{b}_{2},\mathbf{b}_{3},\mathbf{b}_{4})^T\,.
  \end{align}
\end{Example}
\parAfterEnv
%\todo{Eigenvectors if Hermitian Matrix form orthonormal basis.\\ Eigenvectors come from Matrices while Eigenfunction come from Operator. In Quantum Mechanics we use an operator and represent those in a Vector space tus Eigenstates are the physical equivalent of eigenfunctions. Eigenfunctions are Eigenvectors.}
Alice has created the states that make up the density operator on her side, so she is aware of that concrete operator.
However, she still has to find the eigenstates, needed to calculate the cheating operation.
Decomposing the density operator into eigenstates has the same complexity as matrix multiplication~\autocite{demmelFastLinearAlgebra2007}.
To calculate the basis conversion \(S\), which is also the cheating transformation operation, a matrix multiplication has to be performed, which for a system of dimension \(k\) has a complexity of \(\mathcal{O}(k^\omega)\), \(2 < \omega < 2.37369\)~\autocite{davieImprovedBoundComplexity2013}.
\mypar
In the general case, if the honest protocol requires \(n\)-bit (classical) string to be transferred ({\(\mathbf{w}\)} in the BB84 scheme), the Hilbert Space on dishonest Alice's side is of dimension \(k = 2^n\).
Thus, the complexity of transforming \(\ket{\psi^{\A}_{0,\gamma}}\) into \(\ket{\psi^{\A}_{1,\gamma}}\) is \(\mathcal{O}(2^{n^\omega} + 2^{n^\omega}) = \mathcal{O}(2^{n^\omega}) = \mathcal{O}(2^{n\omega}),\quad 2 < \omega < 2.37369\).
%\todo{In the non-perfect case, in order to apply Uhlmann's theorem, Alice has to find such a state \(\ket{\psi_{01}}\), this means she has to solve a maximization problem}

\subsubsection{Finding the Cheating State}
To find the purification \(\ket{\psi_{01}}\) described in Equation~\ref{eq:psi01andpsi1}, no maximization problem has to be solved.
Instead, the proof by~\textcite[section 4]{jozsaFidelityMixedQuantum1994} describes how to construct this state.
First~\hyperref[col:schmidt]{Schmidt polar forms} of \(\ket{\psi_0}\) and \(\ket{\psi_{01}}\) are computed:
\begin{align}
  \ket{\psi_0} = \sum_{i = 1}^{k} \sqrt{\lambda_i}\ket{e_i}\ket{f_i}
\end{align}
and
\begin{align}
  \ket{\psi_{01}} = \sum_{i = 1}^{k} \sqrt{\mu_i}\ket{\tilde{e}_i}\ket{g_i}\,.
\end{align}
This is possible, even though \(\ket{\psi_{01}}\) is not yet known, since the Schmidt polar form is given by the eigenvalues \(\lambda_i, \mu_i\) and the orthonormal eigenvectors \(\ket{e_i},\ket{\tilde{e}_i}\) of \(\rho_0\) and \(\rho_1\) respectively.
Then matrices are calculated that transform the different orthonormal bases into each other,
\begin{align}
  \ket{\tilde{e}_i} = V\ket{e_i},\quad
  %\ket{f_i} = U_0\ket{e_i},\quad
  \ket{g_i} = U_1\ket{\tilde{e}_i}\,.
\end{align}
And since \(\ket{e_i},\ket{\tilde{e}_i}\) are eigenvectors of \(\rho_0\) and \(\rho_1\) respectively
\begin{align}
  \sqrt{\lambda_i}\ket{e_i} = \sqrt{\rho_0}\ket{e_i},\quad \sqrt{\mu_i}\ket{e_i} = \sqrt{\rho_1}\ket{\tilde{e}_i}\,.
\end{align}
Using~\textcite[Lemma 7]{jozsaFidelityMixedQuantum1994}, it then can be shown analogous to~\textcite[Section 4]{jozsaFidelityMixedQuantum1994}, that \(\ket{\psi_{01}}\) can be constructed as follows
\begin{align}
  \ket{\psi_{01}} = \sum_{i = 1}^{k}\sqrt{\rho_1}U_1^{T}VV^{T}\ket{e_i}\otimes{}\ket{e_i}\,.
\end{align}
%Also following~\textcite[section 4]{jozsaFidelityMixedQuantum1994}, it can be shown, that \(\ket{\psi_0}\) can be written as
%\begin{align}
%  \ket{\psi_{0}} = \sum_{i = 1}^{k}\sqrt{\rho_0}U_0^{T}\ket{e_i}\otimes{}\ket{e_i}\,,
%\end{align}
The eigen-decomposition and basis transforming operator can be reused to find the operator \(S\), that maps \(\ket{\psi_0}\) into \(\ket{\psi_{01}}\), following Subsection~\ref{subsec:transforming-zero-to-one}.
%\begin{align}
%  \ket{\psi_{01}} = \sqrt{\rho_1}U_1^{T}\sqrt{\rho_0}^{-1}U_0^{*}V^{*}V^{\dagger}\ket{\psi_0}\,.
%\end{align}
\mypar
Thus \(\ket{\psi_{01}}\) and \(S\) can be found by calculating eigen-decompositions and basis transformations.
The complexity to do these operations, where the honest protocol requires the transfer of \(n\) bits, was already shown to be \(O(2^{n\omega}),\quad2 < \omega < 2.37369\) in~\ref{subsubsec:complexity-of-the-transformation}.

