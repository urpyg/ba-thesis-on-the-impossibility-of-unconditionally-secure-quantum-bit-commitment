\section{Attack on Generalized QBC}\label{sec:general-cheat}
It is assumed that initially all quantum registers are set to \(\ket{\mathbf{0}}\), as {\qbc} with pre-shared entangled states are practically of no use, as trust in such a state would have to be assumed which defeats the purpose of a {\qbc} or {\bc}\footnotemark.
\footnotetext{For the no-go theorem that is presented here and was presented by~\textcite{mayersTroubleQuantumBit1996,loQuantumBitCommitment1997}, it is sufficient to assume that no pre-existing shared entanglement exists.
The assumption, that all quantum registers are set to \(\ket{\mathbf{0}}\) at the beginning of the protocol follows~\textcite{mayersTroubleQuantumBit1996}, while~\textcite{loQuantumBitCommitment1997} assumes that no entanglement is pre-shared, and the system starts in a pure state.
Both of these assumptions can be lifted to also include non-static {\qbc}-protocols, so protocols where the state of Bob's system at the start of the protocol is not known to Alice.
This is shown by~\textcite{Li2011} without changing actual the proof.
However for the sake of readability Mayer's assumption is kept in this thesis.}
\mypar
Let \(\commit^{\A}, \unveil^{\A}, \ket{\psi^{\A}}\) modified by dishonest Alice, \(\commit^{\B}, \ket{\psi^{\B}}\) modified by dishonest Bob.
While looking at one cheating party, it is assumed that the other party is bund to a fixed honest strategy.
\mypar
As many notations presented throughout this thesis will be combined in this section, a summary of these notations is given here.
Everything inside a ket is part of the label for that state vector.
A different label usually means a different state.
Every sub- and superscript outside the ket describes attributes that state has.
\begin{align}
  \ket{\psi}_{\theta}^{\H}\label{eq:notation:state}\\
  \ket{\psi^{\mathcal{P}}_{b,\xi}}\label{eq:notation:commitment}
  %\ket{\psi^{\mathcal{P}}_{b,\xi}}_{\theta}^{\H}
\end{align}
State~\ref{eq:notation:state} is a state with label \(\psi\), encoded in the basis \(\theta\), that acts on the Hilbert space \(\H\).
State~\ref{eq:notation:commitment} is a commitment, encoding \(b\), created by dishonest party \(\mathcal{P}\), which has stored \(\xi\) in their classical registers.

\subsection{Scheme is Secure Against Bob}\label{subsec:scheme-is-secure-against-bob}
A cheating Bob follows a modified \(\commit\) procedure, \(\commit^{\B}\), in which he never sends a register away to the environment.
Denote \(\eta = \xi_B, \xi_S\) the classical information stored on Bob's side and \(\gamma\) the string of transmitted bits, stored in \(\H_S\) after \(\commit^{\B}\).
Since Bob does not send anything to the environment, \(\xi_B\) is the empty string and \(\eta = \gamma\).
For the same reason, the state \(\ket{\psi^{\B}_{b,\eta}}\) that is produced in \(\commit^{\B}\) is the state of the system \({\H}_{E,{\A}}\otimes{}{\H}_{\mathcal{A}}\otimes{}{\H}_{\mathcal{B}}\).
Bob succeeds in cheating if he is able to gain some information about \(b\) before the \(\unveil\)-phase.
To be more precise, he succeeds if he is able to break the \emph{concealing}-property of the scheme.
Following Definition~\ref{def:concealing_qbc}, for the scheme to be secure against Bob, the fidelity of the reduced density operators on Bob's side has to be
\begin{align}
    F''\left(\eta\right) \coloneqq F\left(\rho_{\B}\left(\ket{\psi^{\B}_{0,\eta}}\right), \rho_{\B}\left(\ket{\psi^{\B}_{1,\eta}}\right)\right) = 1 - \delta,\quad\delta \geq 0\,,
\end{align} with \(\delta\) close to zero.
\mypar
For this insecurity proof it is assumed that the scheme is secure against Bob because otherwise the scheme would already be insecure.
\subsection{Security Against Bob Implies Insecurity Against Alice}\label{subsec:security-against-bob-implies-insecurity-against-alice}
As in the attack on BB84, without loss of generality, cheating Alice in \(\commit^{\A}\) chooses \(b = 0\) and does not send any registers to the environment, with the exception of classical bits she is required to transfer to Bob (using a non quantum channel).
\mypar
\(\gamma\) is the classical, random string stored in \(\H_S\) after \(\commit^{\A}\) and is in fact the same string as the one stored in \(\H_{S}\) after \(\commit^{\B}\).
\(\ket{\psi^{\A}_{b,\gamma}}\) is the collapsed state of the commitment.
This is the state of the remaining system \({\H}_{E,{\B}}\otimes{\H}_{\A}\otimes{}{\H}_{\mathcal{B}}\), as \({\H}_{E,{\A}}\) is not used in \(\commit^{\A}\).
Thus, the reduced density matrix on Bob's side \(\rho_{\B}\left(\ket{\psi^{\A}_{b,\gamma}}\right)\) is the state of system \({\H}_{\B}\otimes{}{\H}_{E,{\B}}\).

%Similarly, a dishonest Bob does never send a register away to the environment in his \(\commit^{\B}\) and so \({\H}_{E,{\B}}\) is never used there and the respective state \(\ket{\psi^{\B}_{b,\gamma}}\) is of \({\H}_{E,{\A}}\otimes{}{\H}_{\mathcal{A}}\otimes{}{\H}_{\mathcal{B}}\).\\
\begin{Lemma}\label{lemma:fidelity_close_to_one}
%\todo[size=\tiny]{Collapsing \(H_S\) of the total system this notation lacks \({\H}_{E,{\A}}\), but since this contains untransmitted classical data from Alice it is not of concern. Mention this?}
If the scheme is perfectly or unconditionally secure against Bob
the expected value of the fidelity
\begin{equation}\label{eq:fidelityfdash}
F'(\gamma) \coloneqq F\left(\rho_{\B}\left(\ket{\psi^{\A}_{0,\gamma}}\right),\rho_{\B}\left(\ket{\psi^{\A}_{1,\gamma}}\right)\right)
\end{equation}
is equal to, or respectively arbitrarily close to, 1.
\end{Lemma}
\begin{Proof}
  \(\ket{\psi^{\A}_{b,\gamma}}\) and  \(\ket{\psi^{\B}_{b,\gamma}}\) are formally identical.
  They can be expressed as follows:
  \begin{alignat}{2}
    \label{eq:cheatingStates}
    \ket{\psi^{\A}_{b,\gamma}} &= \sum_{\xi_S,\xi_{\A},\xi_{\B}}\alpha_{(\xi_S,\xi_{\A},\xi_{\B})} {\ket{\xi_S}^{{\H}_{S}}}{\ket{\xi_{\A}}^{{\H}_{\A}'}}{\ket{\xi_{\B}}^{{\H}_{E,\B}}}{\ket{\psi}}^{{\H}_{\A}\otimes{}{\H}_{\B}}\\
    \ket{\psi^{\B}_{b,\gamma}} &= \sum_{\xi_S,\xi_{\A},\xi_{\B}}\alpha_{(\xi_S,\xi_{\A},\xi_{\B})} {\ket{\xi_S}^{{\H}_{S}}}{\ket{\xi_{\A}}^{{\H}_{E,{\A}}}}{\ket{\xi_{\B}}^{{\H}_{\mathcal{B}}''}}{\ket{\psi}}^{{\H}_{\mathcal{A}}\otimes{}{\H}_{\mathcal{B}}}\,,
  \end{alignat}
  where \({\H}_{\mathcal{A}}'\) is the subsystem of \(\H_A\), that replaces \({\H}_{E,{\A}}\) in cheating Alice's \(\ket{\psi^{\A}_{b,\gamma}}\) and \({\H}_{\mathcal{B}}''\) is the subsystem of \(\H_{\B}\), that replaces \({\H}_{E,{\B}}\) in cheating Bob's \(\ket{\psi^{\B}_{b,\gamma}}\).
  \(\rho_{\B}(\ket{\cdot})\) is the reduced density operator of Bob's systems.
  This means all of Alice's systems are traced out.
  \mypar
  It will now be shown that the reduced density operators on Bob's side after \(\commit^{\A}\) and after \(\commit^{\B}\) are also formally identical.
  \begin{alignat}{3}\label{eq:partialtraces3}
  \rho_{\B}&\left(\ket{\psi^{\A}_{b,\gamma}}\right)&&\\
  = &\Tr_{{\H}_{\A}\otimes{}{\H}_{E,{\A}}}&&\left(\ket{\psi^{\A}_{b,\gamma}}\bra{\psi^{\A}_{b,\gamma}}\right) \nonumber\\
  = &\Tr_{{\H}_{\A}\otimes{}{\H}_{E,{\A}}}&&\left(\left({\sum_{\xi_S,\xi_{\A},\xi_{\B}}\alpha {\ket{\xi_S}^{{\H}_{S}}}{\ket{\xi_{\A}}^{{\H}_{\mathcal{A}}'}}{\ket{\xi_{\B}}^{{\H}_{E,{\B}}}}{\ket{\psi}}^{{\H}_{\mathcal{A}}\otimes{}{\H}_{\mathcal{B}}}}\right)\right.\nonumber\\
  &&&\left.\left({\sum_{\xi_S,\xi_{\A},\xi_{\B}}\alpha^* {\bra{\xi_S}^{{\H}_{S}}}{\bra{\xi_{\A}}^{{\H}_{\mathcal{A}}'}}{\bra{\xi_{\B}}^{{\H}_{E,{\B}}}}{\bra{\psi}}^{{\H}_{\mathcal{A}}\otimes{}{\H}_{\mathcal{B}}}}\right)\right)\nonumber\\
 = &\sum_{\xi_S,\xi_{\A},\xi_{\B}}\alpha\alpha^*&&\nonumber\\
  &\left(\Tr_{{\H}_{\A}\otimes{}{\H}_{E,{\A}}}\right.&&\left.\left({\ket{\xi_S}}{\bra{\xi_S}^{{\H}_{S}}}\otimes{\ket{\xi_{\A}}}{\bra{\xi_{\A}}^{{\H}_{\mathcal{A}}'}}\otimes{\ket{\xi_{\B}}}{\bra{\xi_{\B}}^{{\H}_{E,{\B}}}}\otimes{\ket{\psi}}{\bra{\psi}}^{{\H}_{\mathcal{A}}\otimes{}{\H}_{\mathcal{B}}}\right)\right)\nonumber\\
 = &\sum_{\xi_S,\xi_{\A},\xi_{\B}}\alpha\alpha^*&&\left({\ket{\xi_S}}{\bra{\xi_S}^{{\H}_{S}}}\otimes{\ket{\xi_{\B}}}{\bra{\xi_{\B}}^{{\H}_{E,{\B}}}}\otimes\Tr_{{\H}_{\A}}\left({\ket{\psi}}{\bra{\psi}}^{{\H}_{\mathcal{A}}\otimes{}{\H}_{\mathcal{B}}}\right){\braket{\xi_{\A}\vert\xi_{\A}}}\right)\label{eq:lem1:A}\,,
  \end{alignat}
  and
  \begin{alignat}{3}\label{eq:partialtraces2}
  \rho_{\B}&\left(\ket{\psi^{\B}_{b,\gamma}}\right)&&\\
  = &\Tr_{{\H}_{\A}\otimes{}{\H}_{E,{\A}}}&&\left(\ket{\psi^{\B}_{b,\gamma}}\bra{\psi^{\B}_{b,\gamma}}\right) \nonumber\\
  = &\Tr_{{\H}_{\A}\otimes{}{\H}_{E,{\A}}}&&\left(\left({\sum_{\xi_S,\xi_{\A},\xi_{\B}}\alpha {\ket{\xi_S}^{{\H}_{S}}}{\ket{\xi_{\A}}^{{\H}_{E,{\A}}}}{\ket{\xi_{\B}}^{{\H}_{\mathcal{B}}''}}{\ket{\psi}}^{{\H}_{\mathcal{A}}\otimes{}{\H}_{\mathcal{B}}}}\right)\right.\nonumber\\
  &&&\left.\left({\sum_{\xi_S,\xi_{\A},\xi_{\B}}\alpha^* {\bra{\xi_S}^{{\H}_{S}}}{\bra{\xi_{\A}}^{{\H}_{E,{\A}}}}{\bra{\xi_{\B}}^{{\H}_{\mathcal{B}}''}}{\bra{\psi}}^{{\H}_{\mathcal{A}}\otimes{}{\H}_{\mathcal{B}}}}\right)\right)\nonumber\\
  = &\sum_{\xi_S,\xi_{\A},\xi_{\B}}\alpha\alpha^*&&\nonumber\\
  &\left(\Tr_{{\H}_{\A}\otimes{}{\H}_{E,{\A}}}\right.&&\left.\left({\ket{\xi_S}}{\bra{\xi_S}^{{\H}_{S}}}\otimes{\ket{\xi_{\A}}}{\bra{\xi_{\A}}^{{\H}_{E,{\A}}}}\otimes{\ket{\xi_{\B}}}{\bra{\xi_{\B}}^{{\H}_{\mathcal{B}}''}}\otimes{\ket{\psi}}{\bra{\psi}}^{{\H}_{\mathcal{A}}\otimes{}{\H}_{\mathcal{B}}}\right)\right)\nonumber\\
  = &\sum_{\xi_S,\xi_{\A},\xi_{\B}}\alpha\alpha^*&&\left({\ket{\xi_S}}{\bra{\xi_S}^{{\H}_{S}}}\otimes{\ket{\xi_{\B}}}{\bra{\xi_{\B}}^{{\H}_{\mathcal{B}}''}}\otimes\Tr_{{\H}_{\A}}\left({\ket{\psi}}{\bra{\psi}}^{{\H}_{\mathcal{A}}\otimes{}{\H}_{\mathcal{B}}}\right){\braket{\xi_{\A}\vert\xi_{\A}}}\right)\label{eq:lem1:B}\,.
  \end{alignat}
%  \begin{alignat}{3}
%    = &\Tr_{{\H}_{\A}}&&\left({\sum_{\xi_S,\xi_{\A},\xi_{\B}}\alpha_{(\xi_S,\xi_{\A},\xi_{\B})} {\ket{\xi_S}^{{\H}_{S}}}{\ket{\xi_{\A}}^{{\H}_{\mathcal{A}}'}}{\ket{\xi_{\B}}^{{\H}_{E,{\B}}}}{\ket{\psi}}^{{\H}_{\mathcal{A}}\otimes{}{\H}_{\mathcal{B}}}}\right)\\
%    = &\Tr_{{\H}_{\A}}&&\left({\sum_{\xi_S,\xi_{\A},\xi_{\B}}\alpha_{(\xi_S,\xi_{\A},\xi_{\B})} {\ket{\xi_S}^{{\H}_{S}}}{\ket{\xi_{\B}}^{{\H}_{E,{\B}}}}{\ket{\psi}}^{{\H}_{\mathcal{A}}\otimes{}{\H}_{\mathcal{B}}}}\right)\,,
%  \end{alignat}
%  and
%  \begin{alignat}{3}
%    &\Tr_{{\H}_{\A}\otimes{}{\H}_{E,{\A}}}&&\left(\ket{\psi^{\B}_{b,\gamma}}\right)\\
%    = &\Tr_{{\H}_{\A}\otimes{}{\H}_{E,{\A}}}&&\left(\sum_{\xi_S,\xi_{\A},\xi_{\B}}\alpha_{(\xi_S,\xi_{\A},\xi_{\B})} {\ket{\xi_S}^{{\H}_{S}}}{\ket{\xi_{\A}}^{{\H}_{E,{\A}}}}{\ket{\xi_{\B}}^{{\H}_{\mathcal{B}}''}}{\ket{\psi}}^{{\H}_{\mathcal{A}}\otimes{}{\H}_{\mathcal{B}}}\right)\\
%    = &\Tr_{{\H}_{\A}}&&\left({\sum_{\xi_S,\xi_{\A},\xi_{\B}}\alpha_{(\xi_S,\xi_{\A},\xi_{\B})} {\ket{\xi_S}^{{\H}_{S}}}{\ket{\xi_{\B}}^{{\H}_{\mathcal{B}}''}}{\ket{\psi}}^{{\H}_{\mathcal{A}}\otimes{}{\H}_{\mathcal{B}}}}\right)\,,
%  \end{alignat}
  When comparing~\ref{eq:lem1:A} and~\ref{eq:lem1:B}, it can be observed that the density operators of the cheating states on Bob's side are in fact formally identical.
  \begin{equation}\label{eq:densities}
  \rho_{\B}\left(\ket{\psi^{\A}_{b,\gamma}}\right) = \rho_{\B}\left(\ket{\psi^{\B}_{b,\gamma}}\right)\,.
  \end{equation}
  \mypar
  As Bob does not send registers away to the environment, \(\xi_{\B}\) is the empty string in \(\commit^{\B}\) and \(\eta = (\xi_{\B},\xi_S) = \xi_S = \gamma\), and thus
  \begin{equation}
    \label{eq:fidelities}
    F''(\eta) = F''(\gamma) = F\left(\rho_{\B}\left(\psi^{\B}_{0,\gamma}\right), \psi^{\B}_{1,\gamma}\right)~\stackrel{\text{\ref{eq:densities}}}{=} F\left(\rho_{\B}\left(\psi^{\A}_{0,\gamma}\right), \psi^{\A}_{1,\gamma}\right) = F'(\gamma)\,.
  \end{equation}
  Since the scheme is asserted to be perfectly or respectively unconditionally secure against Bob, the value of \(F''(\eta)\) has to be equal or respectively arbitrarily close to one and so \(F'(\gamma)\) has to be as well.
\end{Proof}