%! suppress = CiteBeforePeriod
\section{Different Strategies for Bob}\label{sec:different-strategies-for-bob}
The proof of \citeauthor{PhysRevLett.78.3414} and \citeauthor{loQuantumBitCommitment1997}, assumes a fixed honest strategy followed by Bob.
This has been criticized by some who assert, that this assumption of Bob's behavior would lose the generality of the proof.
\mypar
One such skeptic is~\textcite{yuenUnconditionallySecureQuantum2005}, who presents a protocol using so called \emph{anonymous states}, which they claim to be unconditionally secure.
Such skeptics argue, that Mayers' and Lo-Chau's proof is merely a demonstration of the impossibility for Kerckhoffian protocols, so protocols that follow the principle that security of cryptographic protocols should not rely on keeping parts of the algorithm secret.
\mypar
However, an extended version of Mayers' no-go theorem is still applicable.
This was shown by~\textcite{darianoReexaminationQuantumBit2007}.
The Authors extend the no-go theorem to general strategies for both Alice and Bob.
\mypar
\citeauthor{darianoReexaminationQuantumBit2007} make a distinction between protocols and strategies.
Protocols are the framework that regulates the exchange of messages.
Strategies are the particular plans Alice and Bob have for operating their local laboratories.
%
When Bob follows a specified honest strategy \(b_{\star}\), which is publicly known in accordance with Kerckhoff's principle, their proof coincides with the analysis in Mayers, Lo and Chau's proof.
\mypar
Mayers, Lo and Chau treat classical information quantum mechanically and send it over noiseless quantum channels.
In contrast to that, the model of~\citeauthor{darianoReexaminationQuantumBit2007} explicitly allows information transfer over classical channels.
%
As a formalism to explicitly handle classical information in protocols,~\citeauthor{darianoReexaminationQuantumBit2007} identify quantum systems by their observable algebras.
In this formalism, a quantum system with Hilbert space \(\H\) represented by the algebra \(\mathcal{B}(\H)\) of operators on \(\H\).
\mypar
Another formalism used is the \emph{communication tree} to represent different stages of the protocol and their relation to each other.
Every node represents the classical information shared up to that point.
The nodes also indicate whose turn it is by associating the first node to Bob's turn and then alternating between parties' turns.
Branches represent possible signals to be sent.
It is also noted for every classical signal, which kind of quantum system accompanies it.
However, the observable algebras of Alice and Bob's laboratories do not only depend on the node in the communication tree but also on their chosen strategies.
\mypar
Cheating becomes harder for Alice if the protocol requires some exchange of classical information as she no longer has full control over the two commitment states.
Unitaries which introduce superpositions of states which belong to different classical values already sent to Bob are forbidden.
Thus, Alice has to find a cheating unitary for every classical communication history.
\mypar
With those formalisms,~\citeauthor{darianoReexaminationQuantumBit2007} give a general framework for two-party cryptographic protocols, in which they then show that secure quantum bit commitment is impossible.
A protocol that falls out of this setting is also presented.
It relies on decoherence in Bob's lab and explores the distinction between local erasure of information and destruction of quantum correlations.
It will be revisited in~\ref{subsubsec:trusted-decoherence}.
\myparagraph
The soundness and security conditions in the impossibility proof are quantified.
%
Alice's honest strategies for committing 0 or 1, i.e.\ \(a_0\) or \(a_1\), can be distinguished with high probability on Bob's side.
If Alice honestly followed \(a_k\), and Bob's measurement results in \(k\) with a probability \(\geq (1 - \eta)\) for a very small \(\eta \geq 0\), then the protocol is \(\eta\)-verifiable or \(\eta\)-sound.
\mypar
The protocol is \(\varepsilon\)-concealing, if Alice's honest strategies cannot be distinguished (up to an error of \(\varepsilon\)) by Bob before the opening phase.
Probabilities he measures differ by at most \(\varepsilon\), no matter which strategy he follows.
When this condition holds with  \(\varepsilon = 0\), the protocol is perfectly concealing.
\mypar
A pair of cheating strategies \(a^{\#}_0, a^{\#}_1 \) for Alice, such that Bob cannot distinguish \(a_0\) from \(a^{\#}_0\) and \(a_1\) from \(a^{\#}_1\) better than with probability difference \(\delta\), is called a \(\delta\)-cheating strategy.
\(a^{\#}_0\) must be the same as \(a^{\#}_1\) throughout the commitment phase.
Such a \(\delta\)-cheating strategy necessarily has to work against all of Bob's strategies.
\mypar
If no \(\delta\)-cheating strategies exist, the protocol is \(\delta\)-binding.
If this is the case and the protocol has a public opening rule --- this means in the opening phase the participants meet and Alice allows Bob to perform arbitrary measurements in her system --- not even Alice herself could help Bob to tell the difference between her strategies in the opening phase.
%
It is shown, that any protocol that is \(\varepsilon\)-concealing allows \(\delta\)-cheating protocol for Alice with \(\delta \leq 2 \sqrt{\varepsilon}\).
\myparagraph
%
%
%Quantum channels that transform systems described by one Hilbert space to another can be described by a completely positive and unital map
%As can change the underlying system (i.e.\ introducing ancilla s, etc.).
%
%In definition for \(\varepsilon\)-concealing, when this condition holds with  \(\varepsilon = 0\) they sy the protocol is perfectly concealing.\todo{Add similar notion to my definition of concealing?}{}
%
%Anonymous state protocols also covered.
%
In an anonymous state protocol as described by~\textcite{yuenUnconditionallySecureQuantum2005}, Bob sends a system to Alice whose state is only known to him, and which an honest Alice has to use in some way for the creation her commitment.
Such an anonymous state protocol would lead Alice to lack some information such that Uhlmann's theorem still implies existence of a cheating transformation, but the transformation might be unknown to her.
In an anonymous state protocol, Alice effectively chooses not a state, but a channel to encode her commitment.
Thus, Uhlmann's theorem no longer applies and a Stinespring representation is used in place.
\mypar
%Skeptics claim Mayers proof is merely a demonstration for Kerckhoffian protocols (= Protocols following Kerckhoff's Principle).
%
%Result is basen on Stinespring's representation instead of Uhlmann's theorem.
The Stinespring representation generalizes Uhlmann's theorem from quantum states to quantum channels.
States can also be expressed as channels with the one-dimensional input space \(\mathbb{C}\).
This generalization is the basis for the result of~\citeauthor{darianoReexaminationQuantumBit2007}.
\mypar
To not restrict generality of their proof by simplifying assumptions, a large class of strategies are to be considered.
The framework presented poses no restriction to finite dimensional systems or number of rounds.
The only restriction is, that the \emph{expected} number of rounds should be finite.
Arbitrarily many rounds of communication of varying lengths, infinite dimensional local laboratory Hilbert spaces etc., all fit into the framework.
The idea followed for simplifications is that \enquote{obviously inferior methods of analysis for Bob} or \enquote{inferior methods to cheat for Alice} need not be considered.
What an \enquote{obviously inferior strategy} is, is then explicitly defined in their proof.
\mypar
The discussion presented in the article is restricted to {\qbc}-protocols in which concealment is guaranteed for all branches of the communication tree.
Such types of commitment protocols are sometimes referred to as \emph{strong bit commitments}.
In a \emph{weak bit commitment}, Bob may learn the value of the bit as long as Alice receives a message stating the bit value has been disclosed.
Weak bit commitment is argued to also be impossible.
%\mypar
