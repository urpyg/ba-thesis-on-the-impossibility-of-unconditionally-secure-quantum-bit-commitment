\section{Environment}\label{sec:environment}
The environment in which the no-go theorem will be presented assumes that neither party or means of communication are affected by decoherence.
In addition to that, relativistic effects will not be taken into account.
It is also assumed, that parties not following the honest protocols are not bounded by space, time or computational power.
It follows, that they are able to perform any valid quantum mechanical measurement or evolution.
Let Alice be the party sending the commitment and Bob the party receiving it.
%
The entire system is described by a set of subsystems
\begin{equation}
  \label{eq:totalsystem}
  \underbrace{\underbrace{{\H}_{S,\mathcal{A}} \otimes{} {\H}_{S,\mathcal{B}}}_{\H_S }\otimes{} {\H}_{E,\mathcal{A}} \otimes{} {\H}_{E,\mathcal{B}}}_{\H_E}\otimes{}{\H}_\mathcal{A} \otimes{}{\H}_\mathcal{B}
\end{equation}
\({\H}_\mathcal{A}, {\H}_\mathcal{B}\) are two-dimensional quantum registers, belonging to Alice and Bob respectively.
However, Alice and Bob are able to introduce new registers, initialized in the \(\ket{0}\) state.
\begin{itemize}
  \item \(\H_E = {\H}_{S} \otimes{} {\H}_{E,\mathcal{A}} \otimes{} {\H}_{E,\mathcal{B}}\) is the environment.
  \item \(\H_S = {\H}_{S,\mathcal{A}} \otimes{} {\H}_{S,\mathcal{B}}\) stores \emph{transmitted classical} Bits.
  \item \({\H}_{S,\mathcal{A}}\) bits Alice transmitted or received and \({\H}_{S,\mathcal{B}}\) bits Bob transmitted or received.
  \item \({\H}_{E,\mathcal{A}}\) and \({\H}_{E,\mathcal{B}}\) store \emph{untransmitted classical} Bits of Alice and Bob respectively.
  \item \(\ket{\psi_b}\) of \(\H_E\otimes{}{\H}_\mathcal{A} \otimes{} {\H}_\mathcal{B}\) is an encoded commitment.
\end{itemize}

%\(\A\) is a shorthand for all of Alice's systems, and \(\B\) for all of Bob's systems.
\subsection{Measuring a State}\label{subsec:meassuring-a-state}
Let the measured system be in an initial state \(\ket{\phi} = \alpha\ket{\phi_0}+\beta\ket{\phi_1}\).
To execute a binary measurement outcome, participant \(\mathcal{P}\in\{\mathcal{A}\text{lice}, \mathcal{B}\text{ob}\}\) introduces a new quantum register which initially is in the state \(\ket{0}\).
\(\mathcal{P}\) entangles it with the measured system, the new state of the entire system is of the form \(\alpha\ket{0}\ket{\phi_0}+\beta\ket{1}\ket{\phi_1}\).
After that, they send the new register to a measurement apparatus \({\H}_{E,\mathcal{P}}\).
%
This process of entangling \(\ket{\phi}\) with a new register and sending this register to a measuring apparatus is later referred to as \emph{sending \(\ket\phi\) to the environment}.
\mypar
As an external viewer, which is not aware of this measurement outcome, this can be seen as if the measuring apparatus amplifies and stores each component \(\ket{x}\) as a state \(\ket{x}^{\H_{E,\mathcal{P}}}\).
The resulting state is
\begin{equation}\label{eq:measurement_outcome}
\alpha\ket{0}^{\H_{E,\mathcal{P}}}\ket{\phi_0}^{\H_{\mathcal{P}}}+\beta\ket{1}^{\H_{E,\mathcal{P}}}\ket{\phi_1}^{\H_{\mathcal{P}}}\,.
\end{equation}
This is illustrated as a quantum circuit in Figure~\ref{fig:circ:storing_measurements}.
%
However, as the state actually has collapsed into the state \(\ket\xi\), this collapse has to be taken into account when modelling transformations on the new state of the system.
%! suppress = EscapeAmpersand
\begin{figure}[!htb]
  \centering
  \begin{quantikz}[slice label style={inner sep=1pt,anchor=south west}]
    \lstick{$\ket{0}$}&\qw& \targ{}&\qw & \qw& \meter{} & \cw & \cw&\rstick[wires=1]{of \(\H_{E,\mathcal{P}}\)} \\
    \lstick{$\ket{\phi}$}\slice{\(\ket{0}\ket{\phi}\)} & \hphantomgate{very wide}\qw & \ctrl{-1}\slice{\(\alpha\ket{0}\ket{\phi_0}+\beta\ket{1}\ket{\phi_1}\)}  & \hphantomgate{very wide} & \hphantomgate{very wide} & \hphantomgate{wide} \slice{\(\alpha\ket{0}^{\H_{E,\mathcal{P}}}\ket{\phi_0}+\beta\ket{1}^{\H_{E,\mathcal{P}}}\ket{\phi_1}\)} & \hphantomgate{very wide} &\qw&\rstick{of \(\H_{\mathcal{P}}\)}
  \end{quantikz}
  \caption{Quantum Circuit demonstrating how Participant \(\mathcal{P}\) measures a system}
  \label{fig:circ:storing_measurements}
\end{figure}
\mypar
In this simple case, the amplitudes are dependent on whether \(\mathcal{P}\)s measurement outcome was \(0\) or \(1\), thus they are dependent on the occurrence of the bit \(\xi_\mathcal{P}\), and~\ref{eq:measurement_outcome} can be written as
\begin{align}
  \sum_{\xi_\mathcal{P}=0}^{1} \alpha(\xi_\mathcal{P})\ket{\xi_\mathcal{P}}^{\H_{E,\mathcal{P}}}\ket{\phi_{\xi_\mathcal{P}}}^{\H_{\mathcal{P}}}\,.
\end{align}
As multiple measurements accumulate, and as multi qubit-measurements can be modelled likewise, \(\xi_\mathcal{P}\) can also represent a bit-string.
%Measurements with more than one qubit work and are presented likewise, \(\xi_\mathcal{P}\) now being a bit-string.\{NO! When I stick with Mayers' notions, the reason for the bitstring is not multi qubit measurements, but the accumulation of multiple single qubit measurements.}
\subsection{State of the Entire System}\label{subsec:state-of-the-entire-system}

The state of the system \({\H}_{E,\mathcal{P}}\otimes{}{\H}_{\mathcal{P}}\) can always be represented as
\begin{align}
  \sum_{\xi_\mathcal{P}} \alpha(\xi_\mathcal{P})\ket{\xi_\mathcal{P}}^{\H_{E,\mathcal{P}}}\ket{\phi_{\xi_\mathcal{P}}}\,.
\end{align}
%
To represent the transmission of classical bits from Alice to Bob, a transformation is used that maps \(\ket{x}^{(E,\mathcal{A})}\ket{0}^{(E,\mathcal{B})}\) into \(\ket{x}^{(S,\mathcal{A})}\ket{x}^{(S,\mathcal{B})}\).
This is not in conflict with the~\hyperref[thm:no-cloning]{no-cloning theorem}, as the underlying information in this process is classical and states representing classical information are orthogonal to each other.
Analogously to represent the transmission of classical bits from Bob to Alice, a transformation is used that maps \(\ket{0}^{(E,\mathcal{A})}\ket{x}^{(E,\mathcal{B})}\) into \(\ket{x}^{(S,\mathcal{A})}\ket{x}^{(S,\mathcal{B})}\).
This means Alice keeps a record of the transmitted bits and Bob likewise.
So the contents of \({\H}_{S,\mathcal{A}}\) and \({\H}_{S,\mathcal{B}}\) are always the same and the string \(\xi_{S}\) can be used to label bits that have been transmitted between the parties.
%Analogous to the untransmited bits \(\xi_\mathcal{P}\), the transmited bits \(\xi_{S}\)
\mypar
As a result, the total system is always in a state
\begin{equation}
  \label{eq:totalstate}
  \sum_{\xi_S,\xi_\mathcal{A},\xi_\mathcal{B}}\alpha_{(\xi_S,\xi_\mathcal{A},\xi_\mathcal{B})} \ket{\xi_S,\xi_\mathcal{A},\xi_\mathcal{B}}^{\H_S \otimes{} {\H}_{E,\mathcal{A}} \otimes{} {\H}_{E,\mathcal{B}}}\ket{\psi(\xi_S,\xi_\mathcal{A},\xi_\mathcal{B})}^{\H_{\A}\otimes{}\H_{\B}}\,.
\end{equation}
\begin{itemize}
  \item $\ket{\psi(\xi_S,\xi_\mathcal{A},\xi_\mathcal{B})}$ is the state of $\H_\mathcal{A} \otimes{} {\H}_\mathcal{B}$ associated with the occurrence of \(\xi_S,\xi_\mathcal{A}\text{ and }\xi_\mathcal{B}\).
  \item \(\eta = (\xi_\mathcal{B}, \xi_S)\) random classical information available to Bob after encoding, stored in \(\H_S\otimes{}{\H}_{E,\mathcal{B}}\).
  \item \(\ket{\psi_{b,\eta}}\) is the corresponding state of system \({\H}_{E_\mathcal{A}} \otimes{} {\H}_\mathcal{A} \otimes{} {\H}_\mathcal{B}\) after the encoding.
  \item \(\rho_\mathcal{B}(\ket{\psi_ {b,\eta}}) = \Tr_{{\H}_\mathcal{A} \otimes{} {\H}_{E,\mathcal{A}}}(\ket{\psi_ {b,\eta}}\bra{\psi_ {b,\eta}})\) the reduced density operator on \({\H}_\mathcal{B}\) given \(\eta\).
\end{itemize}