\section{BB84}\label{sec:bb84}
BB84 is the name given to a \qbc-protocol first presented on in the conference paper~\citetitle{IEEE1984} by~\textcite{IEEE1984}.
The conference paper was later published in~\citetitle{bennettQuantumCryptographyPublic2014}~\parencite{bennettQuantumCryptographyPublic2014}.
\mypar
The idea of BB84 is to create a commitment by encoding random bits in either the rectilinear or the diagonal basis, and to unveil it by unveiling the random bits used to create the commitment.
To \emph{encode a classical bit \(b\) in the rectilinear or diagonal basis}, means to create a state \(\ket{b}_+\) or \(\ket{b}_\times\) respectively.
%
These bases are used because for \(b = 0,1\), measuring \(\ket{b}_+\) in the diagonal basis yields \(\ket{0}_\times\) and \(\ket{1}_\times\) with equal probabilities, and measuring \(\ket{b}_\times\) in the rectilinear basis yields \(\ket{0}_{+}\) and \(\ket{1}_{+}\) with equal probabilities.
%
The security parameter \(n\) dictates how many random bits will be encoded in this way.
\mypar
In the commit-phase, Alice chooses whether to commit to \(b = 0\) or \(b = 1\) and chooses the diagonal or rectilinear basis respectively.
She then generates \(n\) perfectly random bits \(w_i\), which she encodes in the chosen basis.
%
Bob receives the encoded bits \(\ket{r_i}\) from Alice and measures each of those qubits in a basis that was chosen perfectly random between the rectilinear and the diagonal basis.
An algorithmic description of the commit phase is given by Algorithm~\ref{alg:commit}
\mypar
%
\begin{algorithm2e}[!htbp]
  \caption{commit}
  \label{alg:commit}
  \DontPrintSemicolon
  %\textbf{Alice:}\;\Indp
  \Alice{
    \(b\ \leftarrow\ \{0,1\}\)\;
    \(\theta = b\ ?\quad \times\ \ :\ \ +\)\;
    \For{\(i\in \{1\ldots{}n\}\)}{
      \(w_i \stackrel{\$}{\leftarrow}\{0,1\}\)\label{alg:commit:choose}\;
      \(\ket{r_i} \coloneqq \ket{w_i}_{\theta}\)\;
      \textbf{Alice}\(\stackrel{\ket{r_i}}{\rightarrow}\)\textbf{Bob}\label{alg:commit:state}\;
    }
  \(\mathbf{w} \coloneqq \begin{pmatrix*}w_1&\cdots{}&w_n\end{pmatrix*}^T\)\;
    %\(\ket{\mathbf{r}} \coloneqq \bigotimes_{i = 1}^{n} \ket{r_i}\)\;
    %\textbf{Alice}\(\stackrel{\ket{\mathbf{r}}}{\rightarrow}\)\textbf{Bob}\;
  }
  %\Indm\textbf{Bob:}\;\Indp
  \Bob{
    \(\hat{\theta}_1\ldots\hat{\theta}_n = \hat{\theta} \stackrel{\$}{\leftarrow}\{+,\times\}^n\)\;
    \ForEach{\(r_i\)}{
      \(\hat{w_i} = M_{\hat{\theta}_i}\ket{r_i}\)\;
    }}
  %\(\mathbf{\hat{w}} = \begin{pmatrix*}w_1&\cdots&w_n\end{pmatrix*}^{T}\)\;
  %\(\ket{\psi_{b,(\mathbf{w},\mathbf{\hat{w}})}} \coloneqq \)\;
\end{algorithm2e}
%
In Lines~\ref{alg:commit:choose}--\ref{alg:commit:state} of Algorithm~\ref{alg:commit} the state
\begin{align}\label{eq:bb84_commit_state}
  \ket{\psi_{b,\mathbf{{w}}}} \coloneqq \frac{1}{\sqrt{2}}\left(\ket{0}_\theta^{\H_{E,\mathcal{A}}}\ket{0}_\theta^{\H_{\mathcal{B}}} + \ket{1}_\theta^{\H_{E,\mathcal{A}}}\ket{1}_\theta^{\H_{\mathcal{B}}}\right)\,,
\end{align}
is created.
This is because from an outsider perspective, Line~\ref{alg:commit:choose} can be seen as creating the state \(\ket{0}_{\theta}+\ket{1}_{\theta}\) and measuring it, as described in Section~\ref{sec:environment}.
Abstracted further and with the principle of deferred measurement applied, the Lines~\ref{alg:commit:choose} to~\ref{alg:commit:state} can be seen as first creating a random state, transforming its basis if necessary, and then measuring it in the respective basis.
This is expressed as a quantum circuit in Figure~\ref{fig:circ:bb84-encode}.
\mypar
%! suppress = EscapeAmpersand
\begin{figure}[!htb]
  \centering
  \begin{quantikz}[slice label style={inner sep=1pt,anchor=south west}]
    \lstick{\(\ket{b}\)}&\hphantomgate{}                                        &\qw &\ctrl{1}  &\ctrl{2}   &\qw                                  &\hphantomgate{}&\qw      &\qw                                                                              &\qw&\qw&\\
    \lstick{$\ket{0}$}&\gate{H}\slice{created\\random\\variable}&\qw &\gate{H}&\qw         &\qw\slice{switched to\\diagonal basis}&\qw&\ctrl{1}&\hphantomgate{very wide}                                       &\qw&\qw&\rstick{gets sent to Bob}\\
    \lstick{$\ket{0}$}&\qw                                                                &\qw  &\qw         &\gate{H}&\qw                                                         &\qw &\targ{}&\meter{}\slice{sent\\ new state\\ to environment} &\cw&\cw&\rstick{stays with Alice}
  \end{quantikz}
  \caption{Quantum circuit describing Lines~\ref{alg:commit:choose}--\ref{alg:commit:state} of Algorithm~\ref{alg:commit}}
  \label{fig:circ:bb84-encode}
\end{figure}
%
In the \(\unveil\)-phase, Alice sends Bob the bits \(w_i\) she used to create the encodings in \(\commit\).
Bob knows that for all \(w_j\) that Alice has sent, which do not match his own corresponding measurement results, \(\hat{w}_j\), the basis \(\hat{\theta}_j\) he used for his measurement must not have been the one that Alice used to create \(\ket{r_j}\).
Since Alice used the same basis for all \(w_i\)-encodings, all those \(\hat{\theta}_j\) on Bob's side, where \(\hat{w}_j\) mismatched \(w_j\), have to correspond to the same basis.
So Bob understands the complement of one such \(\hat{\theta}_j\) as the basis Alice chose, and then tests whether the other \(\hat{\theta}_j\) would have resulted in the same basis.
If not all of those \(\hat{\theta}_j\) correspond to the same basis, Alice must have not followed the honest protocol.
\mypar
\begin{algorithm2e}[!htbp]
  \caption{unveil}
  \label{alg:unveil}
  \DontPrintSemicolon
  \textbf{Alice}\ \(\stackrel{\mathbf{w}}{\rightarrow}\)\ \textbf{Bob}\;
  %\textbf{Bob:}\;
  \Bob{
    \(j'\ \leftarrow\ \{j\ :\ w_j \neq \hat{w}_j\}\)\;
    \(\tilde{\theta} \coloneqq\  \begin{cases}+,&\hat{\theta}_{j'} = \times\\\times,&\hat{\theta}_{j'} = +\\\end{cases}\)\;
    \ForEach{\(j\ :\ w_j \neq \hat{w}_j\)}{
      %\tcp{\(\theta \stackrel{!}{\neq} \hat{\theta}_j\)}
      \If{\(\hat{\theta}_{j'} \neq\ \hat{\theta}_j\)}{
        \Return \(\bot\)\;
      }
    }
    \Return \(\tilde{\theta} \stackrel{?}{=} \times\)\;
  }
\end{algorithm2e}
A failure case that can occur is when all of Bob's measurements \(\hat{w}_i\) also match Alice's bits \(w_i\), where Bob chose a different Basis than \(\theta\).
In this case he can derive no information about the basis Alice chose or whether she cheated, and thus the protocol fails.
However, the probability of this happening decreases exponentially with an increase of the security parameter.

\FloatBarrier

\subsection{Defeating BB84}\label{subsec:defeating-bb84}
A dishonest Alice executes honest \(\commit\) for \(b = 0\) but never sends anything to the environment.
Thus, the underlying systems of the State~\ref{eq:bb84_commit_state} changes
\begin{equation}\label{eq:cheatingstaterect}
\ket{\psi_{0,\mathbf{{w}}}'} = 1/\sqrt{2}\left(\ket{0}_+^{\H_{\mathcal{A}}}\ket{0}_+^{\H_{\mathcal{B}}} + \ket{1}_+^{\H_{\mathcal{A}}}\ket{1}_+^{\H_{\mathcal{B}}}\right)\,.
\end{equation}
While this is formally the same state as the one generated in an honest \(\commit\), due to the change of the underlying systems there now exists a unitary transformation on Alice's side to transform~\ref{eq:cheatingstaterect} into
\begin{equation}
  \label{eq:cheatingstatediag}
  \ket{\psi_{1,\mathbf{{w}}}'} = 1/\sqrt{2}\left(\ket{0}_\times^{\H_{\mathcal{A}}}\ket{0}_\times^{\H_{\mathcal{B}}} + \ket{1}_\times^{\H_{\mathcal{A}}}\ket{1}_\times^{\H_{\mathcal{B}}}\right)\,.
\end{equation}
In the BB84 case this transformation is the identity transformation:
\begin{alignat}{3}\label{eq:bb84pluseqdiag}
  \ket{\psi_{0,\mathbf{{w}}}'} &= \frac{1}{\sqrt{2}}&&\left(\ket{0}_+^{\H_{\mathcal{A}}}\ket{0}_+^{\H_{\mathcal{B}}} + \ket{1}_+^{\H_{\mathcal{A}}}\ket{1}_+^{\H_{\mathcal{B}}}\right)\\
  &=  \frac{1}{\sqrt{2}}&&\left(\frac{1}{2}\left(\ket{0}_+^{\H_{\mathcal{A}}}\ket{0}_+^{\H_{\mathcal{B}}} + \ket{0}_+^{\H_{\mathcal{A}}}\ket{1}_+^{\H_{\mathcal{B}}} + \ket{1}_+^{\H_{\mathcal{A}}}\ket{0}_+^{\H_{\mathcal{B}}} + \ket{1}_+^{\H_{\mathcal{A}}}\ket{1}_+^{\H_{\mathcal{B}}}\right)\right.\\
  &&+&\left.\frac{1}{2}\left(\ket{0}_+^{\H_{\mathcal{A}}}\ket{0}_+^{\H_{\mathcal{B}}} - \ket{0}_+^{\H_{\mathcal{A}}}\ket{1}_+^{\H_{\mathcal{B}}} - \ket{1}_+^{\H_{\mathcal{A}}}\ket{0}_+^{\H_{\mathcal{B}}} + \ket{1}_+^{\H_{\mathcal{A}}}\ket{1}_+^{\H_{\mathcal{B}}}\right)\right)\\
  &= \frac{1}{\sqrt{2}}&&\left(\frac{1}{\sqrt{2}}\left(\ket{0}^{\H_{\mathcal{A}}}_{+}+\ket{1}^{\H_{\mathcal{A}}}_{+}\right)\otimes{}\frac{1}{\sqrt{2}}\left(\ket{0}^{\H_{\mathcal{B}}}_{+}+\ket{1}^{\H_{\mathcal{B}}}_{+}\right)\right.\\
  &&+& \left.\frac{1}{\sqrt{2}}\left(\ket{0}^{\H_{\mathcal{A}}}_{+}-\ket{1}^{\H_{\mathcal{A}}}_{+}\right)\otimes{}\frac{1}{\sqrt{2}}\left(\ket{0}^{\H_{\mathcal{B}}}_{+}-\ket{1}^{\H_{\mathcal{B}}}_{+}\right)\right)\\
  &= \frac{1}{\sqrt{2}}&&\left(\ket{0}_\times^{\H_{\mathcal{A}}}\ket{0}_\times^{\H_{\mathcal{B}}} + \ket{1}_\times^{\H_{\mathcal{A}}}\ket{1}_\times^{\H_{\mathcal{B}}}\right) = \ket{\psi_{1,\mathbf{{w}}}'}\,.
\end{alignat}
%
How this allows Alice to cheat is demonstrated in the following example.
\begin{Example}[Attack on BB84, four qubit case]\label{ex:bb84-n4}
Let \(n = 4\).
Alice chooses \(b = 0\).
%
Instead of choosing random \(w_1\ldots{}w_4 \leftarrow \{0,1\}^4\) and creating \(\ket{r_i} = \ket{w_i}_{+}\), Alice creates the EPR pairs
\begin{align}
    \ket{h_i} = \frac{1}{\sqrt{2}}\left(\ket{00}_{+}+\ket{11}_{+}\right), i = 1,\ldots,4\,.
\end{align}
For each pair she labels one bit \(\ket{r_i}\) and sends those \(\ket{r_i}\) to Bob, keeping the other registers.
%
Let Bob choose the Bases
\begin{align}
    \mathbf{\hat{\theta}} = \begin{pmatrix}+\\+\\\times\\\times\end{pmatrix}\,,
\end{align}
and let him measure
\begin{align}
    \mathbf{\hat{w}} = 0101\,.
\end{align}
This is the end of the commit procedure.
\mypar
\(\ket{00}_\theta\) and \(\ket{11}_\theta\) are the only possible measurement outcomes, when measuring  \(\ket{00}_\theta \pm \ket{11}_\theta\) in the \(\theta\) basis.
This means, the EPR pairs have collapsed to
\begin{align}
    \left(\ket{00}_+,\ket{11}_+, \ket{00}_\times, \ket{11}_\times\right)\,.
\end{align}
If Alice does not want to change her commitment, she does nothing to her state and measures it in the rectilinear basis.
For \(i = 1, 2\) this is the same basis Bob has measured his qubits in, and since the first two EPR pairs have collapsed to \(\ket{00}_+\) (for \(i = 1\)) and \(\ket{11}_+\) (for \(i = 2\)) respectively, she measures \(\ket{0}_+\) and \(\ket{1}_+\) respectively on her bits and receives the same bits Bob has measured:
\begin{align}
    w_{1,2} = \hat{w}_{1,2} = 01\,.
\end{align}
For \(i = 3\) Bob measured \(\ket{0}_{\times}\), so the total state of this pair has collapsed to \(\ket{00}_{\times} = \frac{1}{2}(\ket{00}_{+}+\ket{01}_{+}+\ket{10}_{+}+\ket{11}_{+})\).
Since Alice measures in the rectilinear base, she receives \(0\) or \(1\) with equal probabilities, so a random bit \(w_3\).
\mypar
Similarly for \(i = 4\), Bob measured \(\ket{r_4}\) in the diagonal basis and since \(\ket{11}_{\times} = \frac{1}{2}(\ket{00}_{+}+\ket{01}_{+}-\ket{10}_{+}-\ket{11}_{+})\), Alice receives a random bit again.
%
This means, there are four possibilities for \(\mathbf{w}\)
\[\mathbf{w} = \begin{cases}
                 0100 \eqqcolon \mathbf{w}^{(1)}\\
                 0101\eqqcolon \mathbf{w}^{(2)}\\
                 0110 \eqqcolon \mathbf{w}^{(3)}\\
                 0111 \eqqcolon \mathbf{w}^{(4)}\,.
\end{cases}\]
She sends her \(\mathbf{w}\) to Bob and Bob looks at the \(i\) where \({w}_i \neq {\hat{w}_i}\)
\begin{alignat}{3}
  w_4^{(1)} &\neq \hat{w}_4 &\Rightarrow\quad& \theta = +, b = 0\\
  \mathbf{w}^{(2)} &= \mathbf{\hat{w}} &\Rightarrow\quad& \bot\\
  w_{3,4}^{(3)} &\neq \hat{w}_{3,4} \land \hat{\theta}_3 = \hat{\theta}_4 &\quad\Rightarrow\quad& \theta = +, b = 0\\
  w_3^{(4)} &\neq \hat{w}_3 &\Rightarrow\quad& \theta = +, b = 0\,.
\end{alignat}
Where the inconclusive result is the same that would have occurred in an honest scenario where Bob would have measured by chance the same string that Alice has chosen.
%
So Alice succeeds in convincing Bob she was always committed to \( b= 0\).
\mypar
If Alice wants to change her commitment to \(b= 1\), she \enquote{transforms} \(\ket{\psi_{0,\mathbf{\hat{w}}}'}\) into \(\ket{\psi_{1,\mathbf{\hat{w}}}'} = \frac{1}{\sqrt{2}}(\ket{00}_{\times} + \ket{11}_\times)\) by measuring it in the diagonal basis instead of the rectilinear one.
%
Analogous to above, for \(i = 3, 4\) this is the same basis as the one Bob has measured his qubit in, and since \(\ket{00}_{\times}\) and  \(\ket{11}_{\times}\) are the only possible outcomes, she receives the same bits as Bob has measured
\begin{align}
    w_{3,4} = \hat{w}_{3,4} = 01\,.
\end{align}
For \(i = 1,2\) Bob measured in the rectilinear basis, and since Alice measures in the diagonal basis, she receives random bits \(w_{3,4}\).
\mypar
As above, the bits she measured in the same basis as Bob (diagonal) are the same as Bob's and thus are not looked at.
All bits that can differ from bits Bob measured, belong to Bob's rectilinear basis measurements and thus Bob is convinced that Alice really committed \(b = 1\).
Effectively she cheated by delaying her measurement until after the commit phase and indirectly transforming the commitment state.
\end{Example}
